
// -----------------------------------------------------------------------------
// CRC16 calculator
crcTable_ <- [
    0, 4129, 8258, 12387, 16516, 20645, 24774, 28903, 33032, 37161, 41290, 45419, 49548, 53677, 57806, 61935, 4657,
    528, 12915, 8786, 21173, 17044, 29431, 25302, 37689, 33560, 45947, 41818, 54205, 50076, 62463, 58334, 9314,
    13379, 1056, 5121, 25830, 29895, 17572, 21637, 42346, 46411, 34088, 38153, 58862, 62927, 50604, 54669, 13907,
    9842, 5649, 1584, 30423, 26358, 22165, 18100, 46939, 42874, 38681, 34616, 63455, 59390, 55197, 51132, 18628,
    22757, 26758, 30887, 2112, 6241, 10242, 14371, 51660, 55789, 59790, 63919, 35144, 39273, 43274, 47403, 23285,
    19156, 31415, 27286, 6769, 2640, 14899, 10770, 56317, 52188, 64447, 60318, 39801, 35672, 47931, 43802, 27814,
    31879, 19684, 23749, 11298, 15363, 3168, 7233, 60846, 64911, 52716, 56781, 44330, 48395, 36200, 40265, 32407,
    28342, 24277, 20212, 15891, 11826, 7761, 3696, 65439, 61374, 57309, 53244, 48923, 44858, 40793, 36728, 37256,
    33193, 45514, 41451, 53516, 49453, 61774, 57711, 4224, 161, 12482, 8419, 20484, 16421, 28742, 24679, 33721,
    37784, 41979, 46042, 49981, 54044, 58239, 62302, 689, 4752, 8947, 13010, 16949, 21012, 25207, 29270, 46570,
    42443, 38312, 34185, 62830, 58703, 54572, 50445, 13538, 9411, 5280, 1153, 29798, 25671, 21540, 17413, 42971,
    47098, 34713, 38840, 59231, 63358, 50973, 55100, 9939, 14066, 1681, 5808, 26199, 30326, 17941, 22068, 55628,
    51565, 63758, 59695, 39368, 35305, 47498, 43435, 22596, 18533, 30726, 26663, 6336, 2273, 14466, 10403, 52093,
    56156, 60223, 64286, 35833, 39896, 43963, 48026, 19061, 23124, 27191, 31254, 2801, 6864, 10931, 14994, 64814,
    60687, 56684, 52557, 48554, 44427, 40424, 36297, 31782, 27655, 23652, 19525, 15522, 11395, 7392, 3265, 61215,
    65342, 53085, 57212, 44955, 49082, 36825, 40952, 28183, 32310, 20053, 24180, 11923, 16050, 3793, 7920
]
    
function crc16(message, length) {
    local remainder = 0xFFFF;
    for (local byte = 0; byte < length; ++byte)
    {
        local data = (message[byte]) ^ (remainder >> 8) & 0x00FF;
        remainder = crcTable_[data] ^ (remainder << 8) & 0xFFFF;
    }
    return remainder;
}
// -----------------------------------------------------------------------------

const DEF_USERNAME       = "\xAB\xAD\xC0\xDE";

const PacketSYNC         = 0xC0;    // SYNC and ESC are for syncing information
const PacketESC          = 0xDB;    // ESC code for encoding bytes
const PacketNotLast      = 0x02;    // not the last packet in a block of data
const PacketLast         = 0x03;    // the last packet in the block of data
const PacketPayLength    = 0x32;    // maximum datasize for a packet
const PacketMaxCount     = 0x0D;    // maximum number of packets in a command
const PacketSequenceMask = 0x1F;    // mask to get out sequence number from packet
const PacketTypeShift    = 0x05;    // bits to shift to get type out
const PacketDataStart    = 0x03;    // offset where data starts in a packet
const PacketOverhead     = 0x05;    // bytes overhead on data per packet
const PacketDestLoc      = 0x02;    // byte with packet destination in header

const PacketErrorNone     = 0x00; // no error
const PacketErrorTimeout  = 0x01; // too long between packets
const PacketErrorMissing  = 0x02; // missing packet in sequence
const PacketErrorChecksum = 0x03; // Packet had a bad checksum (CRC)
const PacketErrorType     = 0x04; // type illegal
const PacketErrorSequence = 0x05; // out of sequence counter
const PacketErrorSYNC     = 0x06; // SYNC characters missed
const PacketErrorLength   = 0x07; // data inconsistency
const PacketErrorCommand  = 0x08; // illegal command
const PacketErrorData     = 0x09; // command legal, data illegal
const PacketErrorDecode   = 0x0A; // decoding of ESC failed
const PacketErrorOverflow = 0x0B; // too much data fed the packet
const PacketErrorNotImpl  = 0x0C; // item not implemented

// version 0.3
const CommandLogin        = 0x00;   
const CommandLogout       = 0x01;
const CommandVersion      = 0x0C;
const CommandAck          = 0x19;
const CommandPing         = 0x3C;
const CommandError        = 0x14;
// version 0.4
const CommandSetFrame     = 0x51;
const CommandFlipFrame    = 0x50;
// version 0.5
const CommandReset        = 0x0A;
const CommandOptions      = 0x0F;
const CommandGetError     = 0x15;
const CommandInfo         = 0x0B;
// version 0.6
const CommandMaxVisIndex  = 0x28;
const CommandSelectVis    = 0x29;
const CommandMaxTranIndex = 0x2A;
const CommandSelectTran   = 0x2B;
const CommandGetFrame     = 0x52;
// version 0.7 - Nothing below is implemented yet.
const CommandSetPixel     = 0x54;
const CommandGetPixel     = 0x55;
const CommandCurrentItem  = 0x1E;
// version 0.8
const CommandSetRate      = 0x32;
const CommandDrawLine     = 0x56;
const CommandDrawBox      = 0x57;
const CommandFillImage    = 0x58;
// version 0.9
const CommandSetPFrame    = 0x53;
const CommandScrollText   = 0x59;
const CommandLoadAnim     = 0x5A;
    
    
// -----------------------------------------------------------------------------
function send(command, data = "", destination = 0) {
    
	data = command.tochar() + data; // Prepend the command to the data

    local buffer = blob(100);
    local length = data.len();
	local sequence = 0;
	local i = 0;
        
    while (length > 0) {
        local dest = 0;
        
        // compute length of data to send this packet
        local curLength = length;
        if (curLength > PacketPayLength)
            curLength = PacketPayLength; // maximum length
        length -= curLength; // remaining is amount for later packets
        
		// Byte 1 - type and sequence
		local type = (length == 0) ? PacketLast<<5 : PacketNotLast<<5;
        buffer[dest++] = type | (sequence & PacketSequenceMask);
                
		// Byte 2 - length
        buffer[dest++] = curLength & 0xFF;

		// Byte 3 - destination
        buffer[dest++] = destination & 0xFF;
        
		// Bytes 4... - data
        while (curLength--) {
            buffer[dest++] = data[i++] & 0xFF;            // copy data bytes to packet
        }
        
		// Bytes -2...-1 - crc16
        local crc = crc16(buffer, dest);
        buffer[dest++] = (crc >> 8);  // MSB
        buffer[dest++] = (crc & 0xFF); // LSB    
        
		// Now lets write these out to the uart
        local bufferstr = "";
        uart.write(PacketSYNC);
        for (local pos = 0; pos < dest; ++pos) {
            if (PacketSYNC == buffer[pos]) { 
                // replace with ESC, ESC+1
                uart.write(PacketESC);
                uart.write(PacketESC+1);
            } else if (PacketESC == buffer[pos]) {
                // replace with ESC, ESC+2
                uart.write(PacketESC);
                uart.write(PacketESC+2);
            } else {
                uart.write(buffer[pos]);
            }
            
            bufferstr += format("0x%02x ", buffer[pos]);
        }
        uart.write(PacketSYNC); // final SYNC        
		imp.sleep(0);

        // server.log(format("Command Sent %03d (%3d bytes): %s", sequence, dest, bufferstr));
        ++sequence;
    }
    
}


in_sync <- 0;
receivebuffer <- "";
responsebuffer <- "";
function receive() {
    local b = null;
    while ((b = uart.read()) != -1) {
        
        // Capture the text and commands
        if (!in_sync) {
            if (b == PacketSYNC) {
                in_sync = true;
            } else {
                // Output text if there is any
                if (b != '\n' && b != '\r') {
                    receivebuffer += b.tochar();
                } else {
                    if (receivebuffer.len() > 0 && receivebuffer != "Console disabled" && receivebuffer != "Console enabled") {
                        server.log(format("Message Received (%3d bytes): %s", receivebuffer.len(), receivebuffer));
                    }
                    receivebuffer = "";
                }
            }
        } else {
            if (b == PacketSYNC) {
                in_sync = false;
                
                // Process command
                if (responsebuffer.len() > 0) {
                    // De-escape the buffer 
                    local response = "";
                    local responsestr = "";
                    for (local i = 0; i < responsebuffer.len(); i++) {
                        local ch = responsebuffer[i];                        
                        if (ch == PacketESC) {
                            if (responsebuffer[i+1] == PacketESC+1) {
                                response += PacketSYNC.tochar();
                                responsestr += format("0x%02x ", PacketSYNC);
                                i++;
                            } else if (responsebuffer[i+1] == PacketESC+2) {
                                response += PacketESC.tochar();
                                responsestr += format("0x%02x ", PacketESC);
                                i++;
                            } else {
                                response += ch.tochar();
                                responsestr += format("0x%02x ", ch);
                            }
                        } else {
                            response += ch.tochar();
                            responsestr += format("0x%02x ", ch);
                        }                        
                    }

					if (responsebuffer[3] != CommandAck) {
						server.log(format("Command Response (%3d bytes): %s", responsebuffer.len(), responsestr));
					}
                }
                responsebuffer = "";
            } else {
                responsebuffer += b.tochar(); 
            }
        }
    }
}

// -----------------------------------------------------------------------------
function Login(username = DEF_USERNAME) {
    send(CommandLogin, username);
}

function SetFrame(colors) {
    local data = "";
	for (local i = 0; i < 64; i+=2) {
		data += format("%c%c%c", colors[i] >> 4, (colors[i] & 0xF) << 4 | (colors[i+1] >> 8), colors[i+1] & 0xFF);
	}
    send(CommandSetFrame, data);
}

function FillFrame(color) {
    local data = "";
	for (local i = 0; i < 64/2; i++) {
		data += format("%c%c%c", color >> 4, (color & 0xF) << 4 | (color >> 8), color & 0xFF);
	}
    send(CommandSetFrame, data);
}

function FlipFrame() {
    send(CommandFlipFrame);
}

function Logout() {
    send(CommandLogout);
}

function Ping() {
    send(CommandPing);
}
// -----------------------------------------------------------------------------

frame <- 0;
function RepeatDraw() {
	local colors = [];
	for (local i = 0; i < 64; i++) {
		colors.push(i == frame ? frame*64 : 0x000);
	}
	SetFrame(colors);
	frame = (frame+1) % 64;

	FlipFrame();
    imp.wakeup(0, RepeatDraw);
}



// -----------------------------------------------------------------------------
// Now start
imp.configure("Hypnocube", [], []);
server.log("Device rebooted")

uart <- hardware.uart12;
uart.configure(38400, 8, PARITY_NONE, 1, NO_CTSRTS, receive);

Login();
RepeatDraw();
