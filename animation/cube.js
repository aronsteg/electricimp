
// Setup the global variables
var params = $.url().param();
var agentid = params['agent'].slice(-12);
var fburl = "https://devices.firebaseIO.com/agents/" + agentid + "/";
var fb = new Firebase(fburl);
var tl = new TimelineLite();
const MAXIMUM_DISPLAY_POINTS = 720;

$(function() {

	// Setup the 3d block
	var wrapper = $('#wrapper');
	   TweenLite.set("#cubeHolder", {perspective:1800});
	   TweenLite.set($('#wrapper'), {transformStyle:"preserve-3d"});

	TweenLite.set(['#topface', '#bottomface', '#leftface', '#backface', '#rightface'], {transformOrigin:"50% 50% -100"})
	TweenLite.set('#topface', 	 { rotationX: "90" });
	TweenLite.set('#bottomface', { rotationX: "270" });
	TweenLite.set('#leftface',   { rotationY: "90" });
	TweenLite.set('#rightface',  { rotationY: "-90" });
	TweenLite.set('#backface',   { z:-200 });

	//spin cube around center. transformOrigin: left, top, z
	// tl.to(wrapper, 3, {rotationX:360, rotationY: 180, rotationZ: 24, transformOrigin:"50% 50% -100"})
	//  .to(wrapper, 3, {rotationX:35, rotationY: 145, rotationZ: 245, transformOrigin:"50% 50% -100"})

	// Initialise the database
	fb.on('value', function(snapshot) {

		var accel = snapshot.val();
		if (accel == null) return;

		if ("x" in accel) {
			var x = accel.x * 90;
			var y = accel.y * -90;
			var z = accel.z * 180;
			console.log({x:accel.x, y:accel.y, z:accel.z}, " => ", {x:x, y:y, z:z});

			tl.to(  wrapper, 
					0.3, 
					{ rotationX:x, rotationY:0, rotationZ:y, transformOrigin:"50% 50% -100"}
					);
		} else if ("angle" in accel) {
			tl.to(  wrapper, 
					0.3, 
					{ rotationY:-accel.angle, rotationX:23, transformOrigin:"50% 50% -100"}
					);
		}

	});


    // Initialise the accelerometer
	var lastEventData = null;
    if (window.DeviceOrientationEvent) {
	  window.addEventListener('deviceorientation',
		function (eventData) {
		  if (eventData.gamma == null) return;
		  lastEventData = eventData;
		}, false);


		// Regularly check for updates
		setInterval(function() {
			if (lastEventData === null) return;

			var log = "";
			log += "Gamma (l/r) = " + lastEventData.gamma.toFixed(2);
			log += ", Beta (f/b) = " + lastEventData.beta.toFixed(2);
			log += ", Alpha (dir) = " + lastEventData.alpha.toFixed(2);
			$("#logs").html(log);

			tl.to(  wrapper, 
					0.5, 
					{ rotationY:lastEventData.gamma, rotationX:lastEventData.beta, rotationZ:lastEventData.alpha, transformOrigin:"50% 50% -100"}
					);
		}, 500);
    }

});
