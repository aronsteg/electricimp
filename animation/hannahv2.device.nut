
// IO Expander Class for SX1509
class IoExpander
{
    i2cPort = null;
    i2cAddress = null;
    irqCallbacks = array(16);
 
    constructor(port, address)
    {
        if(port == I2C_12)
        {
            // Configure I2C bus on pins 1 & 2
            hardware.configure(I2C_12);
            i2cPort = hardware.i2c12;
        }
        else if(port == I2C_89)
        {
            // Configure I2C bus on pins 8 & 9
            hardware.configure(I2C_89);
            i2cPort = hardware.i2c89;
        }
        else
        {
            server.log("Invalid I2C port specified.");
        }
 
        i2cAddress = address << 1;
        hardware.pin1.configure(DIGITAL_IN, getIRQSources.bindenv(this));

    }
 
    // Read a byte
    function read(register)
    {
        local data = i2cPort.read(i2cAddress, format("%c", register), 1);
        if(data == null)
        {
            server.log(format("I2C Read Failure at address 0x%02x", i2cAddress >> 1));
            return -1;
        }
 
        return data[0];
    }
 
    // Write a byte
    function write(register, data)
    {
        i2cPort.write(i2cAddress, format("%c%c", register, data));
    }
 
    // Write a bit to a register
    function writeBit(register, bitn, level)
    {
        local value = read(register);
        value = (level == 0)?(value & ~(1<<bitn)):(value | (1<<bitn));        
        write(register, value);
    }
 
    // Write a masked bit pattern
    function writeMasked(register, data, mask)
    {
       local value = read(register);
       value = (value & ~mask) | (data & mask);
       write(register, value);
    }
 
    // Set a GPIO level
    function setPin(gpio, level)
    {
        writeBit(gpio>=8?0x10:0x11, gpio&7, level?1:0);
    }
 
    // Set a GPIO direction
    function setDir(gpio, output)
    {
        writeBit(gpio>=8?0x0e:0x0f, gpio&7, output?0:1);
    }
 
    // Set a GPIO internal pull up
    function setPullUp(gpio, enable)
    {
        writeBit(gpio>=8?0x06:0x07, gpio&7, enable);
    }
 
    // Set GPIO interrupt mask
    function setIrqMask(gpio, enable)
    {
        writeBit(gpio>=8?0x12:0x13, gpio&7, enable);
    }
 
    // Set GPIO interrupt edges
    function setIrqEdges(gpio, rising, falling)
    {
        local addr = 0x17 - (gpio>>2);
        local mask = 0x03 << ((gpio&3)<<1);
        local data = (2*falling + rising) << ((gpio&3)<<1);    
        writeMasked(addr, data, mask);
    }
 
    // Clear an interrupt
    function clearIrq(gpio)
    {
        writeBit(gpio>=8?0x18:0x19, gpio&7, 1);
    }
 
     function setIRQCallBack(pin, func){
        irqCallbacks[pin] = func;
    }
    
    function clearIRQCallBack(pin){
           irqCallbacks[pin] = null;
    }
    
    function getIRQSources(){
        local sourceB = read(0x18);
        local sourceA = read(0x19);
        local irqSources = array(16);
        
        local j = 0;
        for(local z=1; z < 256; z = z<<1){
            irqSources[j] = ((sourceA & z) == z);
            irqSources[j+8] = ((sourceB & z) == z);
            j++;
        }
        
        for(local pin=0; pin < 16; pin++){
            if(irqSources[pin]){
                irqCallbacks[pin]();
                clearIrq(pin);
            }
        }
        
       return irqSources;    //Array of the IO pins and who has active interrupts
    }

    // Get a GPIO input pin level
    function getPin(gpio)
    {
        return (read(gpio>=8?0x10:0x11)&(1<<(gpio&7)))?1:0;
    }
}

 

// Class for the potentiometer on the Hannah board
class Potentiometer extends IoExpander {

    // Constructor
    constructor (port, address) {

    	// Call base constructor
		base.constructor (port, address);
		
		// Enable Potentiometer
		setPin(8, 0);
		setDir(8, 1);
		hardware.pin2.configure(ANALOG_IN);
		
	}
	
	// Gets the current value
	function get () {
		return hardware.pin2.read() / 65535.0;
	}
}

 

 

// Register with the server
imp.configure("Hannah", [], []);
server.log("Device is Hannah with impee_id " + hardware.getimpeeid() + " and mac = " + imp.getmacaddress() );

// Construct a potentiometer and hook it to the LEDs
pot <- Potentiometer(I2C_89, 0x3E);
last_angle <- 9999;
function checkPot() {
    imp.wakeup(0.2, checkPot);
    local potval = pot.get();
    local angle = ((potval * 360) - 180);
    if (math.abs(angle - last_angle) >= 1.0) {
        agent.send("pot", angle.tointeger())
    }
    last_angle <- angle;
}
imp.wakeup(0, checkPot);


