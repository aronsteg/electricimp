<?php

/*****************************************************************************************************************
 *
 * Class: Blessing_Model
 * Owner: electric imp
 * Creator: Aron <aron <at> electricimp.com>
 * Purpose: To demonstrate the basic database requirements for handling the blessing webhook.
 * Date: 30th April, 2013
 * Updated: 30th April, 2013
 *
 */
class Blessing_Model
{
	protected $host = "127.0.0.1";
	protected $user = "root";
	protected $pass = "";
	protected $db   = "blessings";

	protected $_db = null;


	// ------------------------------------------------------------------------------------------------------
	// Method: Constructor
	// Description: Configures and connects to the mysql database. If the database doesn't exist, it will be created.
	//              Note, this is a very simplistic model to demonstrate the webhook function. There are more 
	//              appropriate patterns for high volume production use.
	//  
	public function __construct($host = null, $user = null, $pass = null, $db = null)
	{
		// Grab the parameters
		$this->host = $host ?: $this->host;
		$this->user = $user ?: $this->user;
		$this->pass = $pass ?: $this->pass;
		$this->db   = $db   ?: $this->db;

		// Setup the database connection
		$this->_db = new mysqli($this->host, $this->user, $this->pass);
		if ($this->_db->connect_errno) 
		{
			echo "Failed to connect to MySQL: (" . $this->_db->connect_errno . ") " . $this->_db->connect_error . "<br/>\n";
			die;
		}

		// Create the database and tables if they aren't there already
		if (!$this->_db->select_db($this->db)) 
		{
			echo "Creating the '{$this->db}'' database.<br/>\n";
			$this->_db->query("CREATE DATABASE {$this->db}");
			if (!$this->_db->select_db($this->db)) 
			{
				echo "Unable to connect to new database: {$this->db}<br/>\n";
				die;
			}

			echo "Creating the 'blessings' table.<br/>\n";
			$this->_db->query("CREATE TABLE blessings
							(
								device_id VARCHAR(16),
								success integer(1),
								blessed integer(11),
								KEY device_id (device_id),
								KEY blessed (blessed)
							) ENGINE = InnoDB");
		}
	}


	// ------------------------------------------------------------------------------------------------------
	// Method: get_recent()
	// Description: Queries the database table for the last [5] blessings and returns their data as an array of rows.
	//  
	public function get_recent($limit = 5)
	{
		// Now display the blessings recorded so far
		$result = $this->_db->query("SELECT *
								     FROM blessings
								     ORDER BY blessed DESC
								     LIMIT $limit");
		$rows = array();
		if($result)
		{
			// Cycle through results
			if ($result->num_rows > 0) 
			{
				while ($row = $result->fetch_object())
				{
					$rows[] = $row;
				}
			}

			// Free result set
			$result->close();

		}
		return $rows;

	}


	// ------------------------------------------------------------------------------------------------------
	// Method: insert()
	// Description: Inserts the JSON object provided into the database.
	//  
	public function insert($blessing)
	{
		// Build the query
		$query = sprintf("INSERT INTO blessings
						  SET 	device_id = '%s', 
								success = %d, 
								blessed = %d",
						 $this->_db->real_escape_string($blessing->device_id), 
						 isset($blessing->success) ? ($blessing->success ? 1 : 0) : 1,
						 time());

		// Execute the query
		$this->_db->query($query);
	}
}
