<?php

/*****************************************************************************************************************
 *
 * Owner: electric imp
 * Creator: Aron <aron <at> electricimp.com>
 * Purpose: To demonstrate the use of factory blessing webhooks to collect data about newly blessed devices.
 * Date: 30th April, 2013
 * Updated: 30th April, 2013
 *
 */
require_once "blessing_model.php";

// The blessings model encapsulates the database (mysql) functions for reading and writing blessing data
$model = new Blessing_Model();

// If we have an blessing POST (in raw JSON format), then store it.
$request = file_get_contents("php://input");
if ($request) 
{
	// Decode the JSON into a stdClass
	$blessing = @json_decode($request);
	if ($blessing)
	{
		// We really only require the device_id. Anything else is a bonus
		if (isset($blessing->device_id)) 
		{
			// Insert this blessing into the database
			$model->insert($blessing);
			die("OK");
		}
		else 
		{
			// We didn't get a device_id so we have a problem
			die("Incomplete JSON packet.");
		}
	}
	else
	{
		// The POST data didn't decode from JSON correctly
		die("Invalid JSON packet.");
	}
}


// If we haven't processed any POST data then we should display the recent history to the user.
$blessings = $model->get_recent();
require "blessing_view.php";
