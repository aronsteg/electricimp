<!DOCTYPE HTML>
<html lang="en-US">
    <head>

        <meta charset="UTF-8">
        <meta http-equiv="refresh" content="5">
        <title>Factory Blessings</title>
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    </head>

    <body class="bg_d">

        <h1>Recent factory blessings</h1>
        <ol>
        <?php
            if (!$blessings)
            {
                echo "None.";
            }
            else
            {
                foreach ($blessings as $blessing)
                {
					// Extract the data from the blessing array
					$device_id = $blessing->device_id;
					$success = isset($blessing->success) ? $blessing->success : true;
					$when = $blessing->blessed;
                    $whenft = gmdate("Y-m-d H:i:s\Z", $when);

					// Set the style (color, boldness) based on how old the blessing event is and whether or not it worked.
					$style = "";
                    if ($when+60 >= time()) $style .= "font-weight:bold;";
                    if (!$success) $style .= "color:red;";

					// Output one line per blessing. The "timeago" class automatically updates the display to a worded text in reference to now.
					echo "<li><span style='$style'><time class='timeago' datetime='$whenft'></time>: $device_id</span></li>\n";
                }
            }
        ?>
        </ol>

        <!-- jQuery -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <!-- timeago jQuery plugin -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.1.0/jquery.timeago.min.js"></script>
		<script type='text/javascript'>$("time.timeago").timeago(); </script>

    </body>
</html>
