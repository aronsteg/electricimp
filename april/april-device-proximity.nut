

class i2cbase {
 
    i2c = null;
    baseAddr = null;
    
    // -------------------------------------------------------------------------
    // Constructor
    //
    constructor(_baseAddr) {
        
        // Configure the port
        hardware.i2c12.configure(CLOCK_SPEED_100_KHZ);        
        i2c = hardware.i2c12;
        baseAddr = _baseAddr;        
    }


    // -------------------------------------------------------------------------
    // Reads a string or byte from the i2c hardware at the specified address
    //
    function read(addr, _bytes=1) {
        local ret = i2c.read(baseAddr<<1, addr.tochar(), _bytes);
        if (_bytes == 1 && ret && ret.len() == 1) return ret[0];
        else return ret;
    }
    
    // -------------------------------------------------------------------------
    // Writes a byte to the i2c hardware at hte specified address
    //
    function write(addr, command = null) {
        local packet = addr.tochar();
        if (typeof command == "array") {
            foreach (c in command) {
                packet += c.tochar();
            }
        } else if (typeof command == "integer") {
            packet += command.tochar();
        } else if (typeof command == "string") {
            packet += command;
        }
        return i2c.write(baseAddr<<1, packet);
    }
    

 
}


// -------------------------------------------------------------------------
// Class handles the VCNL4000 Infrared Proximity Breakout from Sparkfun
// For information: http://www.sparkfun.com/products/10901
// This code was adapted from: http://bildr.org/2012/11/vcnl4000-arduino/
//
class vcnl4000 extends i2cbase {
    
    lastAmbient = 0;
    lastProximity = 0;
    callbacks = {};
    
    // -------------------------------------------------------------------------
    // Constructor
    //
    constructor(_baseAddr = 0x13) {
        
        base.constructor(_baseAddr);
        
        // Check the device is online
        local product_id = read(0x81, 1);
        if (product_id == 0x11) {
            server.log("Proximity and ambient sensors are online");
        } else {
            server.log("Proximity and ambient sensors are OFFLINE");
            return;
        }
        
        // Configure the sensors
        write(0x84, 0x0F);  // Configures ambient light measures - Single conversion mode, 128 averages
        write(0x83, 0x0F);  // sets IR current in steps of 10mA 0-200mA --> 200mA
        write(0x89, 0x02);  // Proximity IR test signal freq, 0-3 - 781.25 kHz
        write(0x8A, 0x81);  // proximity modulator timing - 129, recommended by Vishay 
        
        // Reads the initial values
        lastProximity = proximity();
        // lastAmbient = ambient();
        
        // Setup a timer to poll the values in order to detect changes
        imp.wakeup(0, changeDetect.bindenv(this));
        
    }
    
    // -------------------------------------------------------------------------
    // Reads the proximity
    //
    function proximity() {
        // readProximity() returns a 16-bit value from the VCNL4000's proximity data registers
        local val = read(0x80);
        if (typeof val != "integer") return false;
        
        write(0x80, val | 0x08);  // command the sensor to perform a proximity measure
        
        while (!(read(0x80) & 0x20));  // Wait for the proximity data ready bit to be set
        local data1 = read(0x87);
        local data2 = read(0x88);
        if (data1 == null || data2 == null) return null; 
        local data = (data1 << 8) | data2;

        // Send the raw data to the callback
        _callback("rawproximity", data);

        // Now normalise the data to 0 (really close) to 100 (really far)
        local n = 0;
        switch (true) {
            case (data >= 30000):                 n = 0; break;
            case (data >= 10000 && data < 30000): n = 1; break;
            case (data >= 5000 && data < 10000):  n = 2; break;
            case (data >= 3000 && data < 5000):   n = 3; break;
            case (data >= 2550 && data < 3000):   n = 4; break;
            case (data >= 2500 && data < 2550):   n = 5; break;
            case (data >= 2450 && data < 2500):   n = 6; break;
            case (data >= 2400 && data < 2450):   n = 7; break;
            case (data < 2400):                   n = 8; break;
        }

        // server.log("Proximity: " + data + ", n = " + n);
        
        return n;
    }
    
    // -------------------------------------------------------------------------
    // Reads the ambient light
    //
    function ambient() {
        // readAmbient() returns a 16-bit value from the VCNL4000's ambient light data registers
        local val = read(0x80);
        if (typeof val != "integer") return false;
        
        write(0x80, val | 0x10);  // command the sensor to perform a ambient light measure
        
        // Wait for the proximity data ready bit to be set
        while (!(read(0x80) & 0x40));  
        local data1 = read(0x85);
        local data2 = read(0x86);
        if (data1 == null || data2 == null) return null; 
        local data = (data1 << 8) | data2;
        
        
        // Send the raw data to the callback
        _callback("rawambient", data);
        
        // Now normalise the data to 0 (really dark) to 100 (really bright)
        local n = 0;
        switch (true) {
            case (data >= 30000):                 n = 8; break;
            case (data >= 10000 && data < 30000): n = 7; break;
            case (data >= 1000 && data < 10000):  n = 6; break;
            case (data >= 500 && data < 1000):    n = 5; break;
            case (data >= 300 && data < 500):     n = 4; break;
            case (data >= 100 && data < 300):     n = 3; break;
            case (data >= 75 && data < 100):      n = 2; break;
            case (data >= 70 && data < 75):       n = 1; break;
            case (data < 70):                     n = 0; break;
        }
        
        // server.log("Ambient: " + data + ", n = " + n);
        
        return n;
    }
    
    // -------------------------------------------------------------------------
    // Timer event which checks the device for changes in the proximity and ambient light
    //
    function changeDetect() {
        imp.wakeup(0.2, changeDetect.bindenv(this));
        
        // Look for changes in the proximity values
        local newProximity = proximity();        
        if (typeof newProximity == "integer" && math.abs(lastProximity - newProximity) > 2) {
            _callback("proximity", newProximity);
            if (newProximity > lastProximity) _callback("farther", newProximity);
            else if (newProximity < lastProximity) _callback("closer", newProximity);
            
            lastProximity = newProximity;
        }
        
        return; 
        
        // Look for changes in the ambient light values
        local newAmbient = ambient();
        if (typeof newAmbient == "integer" && math.abs(lastAmbient - newAmbient) > 2) {
            _callback("ambient", newAmbient);
            if (newAmbient > lastAmbient) _callback("lighter", newAmbient);
            else if (newAmbient < lastAmbient) _callback("darker", newAmbient);
            
            lastAmbient = newAmbient;
        }
    }
    
    
    // -------------------------------------------------------------------------
    // Calls the callback specified but checks before it does.
    //
    function _callback(event, data) {
        if (event in callbacks && callbacks[event] != null) {
            callbacks[event](data);
        }
    }


    // -------------------------------------------------------------------------
    // Register event handlers.
    // Current events: rawproximity, proximity (change), farther, closer, rawambient, ambient (change), lighter, darker.
    //
    function on(event, callback) {
        callbacks[event] <- callback;
    }
    
}


//------------------------------------------------------------------------------
class blinkM extends i2cbase {
    
    // -------------------------------------------------------------------------
    // Constructor
    //
    constructor(_baseAddr = 0x09) {
                
        base.constructor(_baseAddr);

        // Check the device is online by setting it to black and then reading the result.
        write('n', [0x00, 0x00, 0x00]);
        local color = read('g', 3);
        if (color != null) {
            server.log("BlinkM is online");
        } else {
            server.log("BlinkM is OFFLINE");
            return;
        }
    }
    

    // -------------------------------------------------------------------------
    function setColor(r, g=null, b=null) {
        write('n', [r, g, b]);
    }

    // -------------------------------------------------------------------------
    function setFadeSpeed(f) {
        write('f', f);
    }
    
    // -------------------------------------------------------------------------
    function fadeToColor(r, g=null, b=null) {
        write('c', [r, g, b]);
    }

    // -------------------------------------------------------------------------
    function stopScript() {
        write('o');
    }

    // -------------------------------------------------------------------------
    function runScript(id, count = 1) {
        write('o');
        write('p', [id, count, 0]);
    }
    
    // -------------------------------------------------------------------------
    function setBootProgram(id) {
        write('B', [ id == 0 ? 0 : 1, id, 0, 0, 0]);
    }

}


//------------------------------------------------------------------------------
led <- blinkM();
led.setBootProgram(0);
led.setFadeSpeed(10);
led.stopScript();

vcnl4k <- vcnl4000();
vcnl4k.on("farther", function(proximity) { 
    agent.send("farther", proximity);
});
vcnl4k.on("closer", function(proximity) { 
    agent.send("closer", proximity);
});
vcnl4k.on("proximity", function(proximity) { 
    server.log(proximity);
});
agent.on("farther", function (proximity) {
    led.fadeToColor(0x00, 0x00, 0xFF); 
});
agent.on("closer", function (proximity) {
    led.fadeToColor(0xFF, 0x00, 0x00);        
});
