requests <- []

jpeg_buffer <- null

device.on("jpeg_start", function(size) {
    jpeg_buffer = blob(size);
});

device.on("jpeg_chunk", function(v) {
    local offset = v[0];
    local b = v[1];
    for(local i = offset; i < (offset+b.len()); i++) {
        if(i < jpeg_buffer.len()) {
            jpeg_buffer[i] = b[i-offset];
        }
    }
});

device.on("jpeg_end", function(v) {
    
    server.log("Captured");
        
    // Send the image to the server
    local s = "";
    foreach(chr in jpeg_buffer) {
        s += format("%c", chr);
    }
    jpeg_buffer = null;
    
    local req = http.post("http://buzz-03.smsd.com.au/leddisplay/write.php", {}, s);
    req.sendasync(function (dummy) {
        // Done!
        server.log("Sent");
        
        local url = "http://agent.electricimp.com/IWjATLxB4Av4?url=http://buzz-03.smsd.com.au/leddisplay/capture_t.jpg%3Frand=" + math.rand();
        http.get(url).sendasync(function(dummy){
            // Request the next photo
            if (running) imp.wakeup(2, take_picture);        
        });
    });
    
});

function take_picture() {
    server.log("Snap");
    device.send("take_picture", 1);
}

running <- false;
function control(req, res) {
    running = !running;
    if (running) imp.wakeup(1, take_picture);
    res.send(200, running ? "Started" : "Stopped");
}
http.onrequest(control);


server.log("Agent started! URL is " + http.agenturl());
