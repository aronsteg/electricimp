
const BULBS = 25;
const FPS = 5;

// ----------------------------------------------------------------------------
class colorEffects {
    
    drawBuffer = null;
    updateBufferTimer = FPS;
    aniFrame = 0;
    aniQueue = null;
    aniSequence = null;
    
    
    // ........................................................................
    constructor(_bulbs = BULBS) {
        
        // Initialise the hardware
        hardware.configure(SPI_257);
        hardware.spi.configure(SIMPLEX_TX, 250);

        // Initialise the buffers
        drawBuffer = bulbBuffer(_bulbs);
        renderBuffer();
        aniQueue = queue();
        
        // Start the drawing
        imp.wakeup(0, updateBuffer.bindenv(this));
        
        // Let the agent know we are online and handle incoming requests
        agent.on("pushQueue", aniQueue.push.bindenv(aniQueue));
        agent.on("clearQueue", aniQueue.clear.bindenv(aniQueue));
        agent.send("status", "online");        
    }
    
    // ........................................................................
    function updateBuffer() {
        aniSequence = aniQueue.pop(aniSequence);
        // server.log(aniSequence == null ? "Null" : (aniSequence.animation + ":" + aniSequence.frames));
        
        if (aniSequence != null) {
            
            if (!("animation" in aniSequence)) aniSequence.animation <- "walk";
            if (!("color1" in aniSequence)) aniSequence.color1 <- "white";
            if (!("color2" in aniSequence)) aniSequence.color2 <- "blue";
            if (!("steps" in aniSequence)) aniSequence.steps <- 0;
            if (!("frames" in aniSequence)) aniSequence.frames <- null;
            if (!("speed" in aniSequence)) aniSequence.speed <- FPS;
            if (!("brightness" in aniSequence)) aniSequence.brightness <- 100;
            if (!("new" in aniSequence)) aniSequence.new <- false;
            
            if (aniSequence.new) aniFrame = 0;
            updateBufferTimer = aniSequence.speed;
            
            switch (aniSequence.animation)
            {
                case "fixed":
                    for (local i = 0; i < drawBuffer.bulbs; i++) {
                        drawBuffer.setBulb(i, aniSequence.color1, aniSequence.brightness);
                    }
                    break;
                
                case "walk":
                    for (local i = 0; i < drawBuffer.bulbs; i++) {
                        if ((i+aniFrame) % aniSequence.steps == 0) {
                            drawBuffer.setBulb(i, aniSequence.color1, aniSequence.brightness);
                        } else {
                            drawBuffer.setBulb(i, aniSequence.color2, aniSequence.brightness);
                        }            
                    }
                    break;
                    
                case "twinkle":
                    for (local i = 0; i < drawBuffer.bulbs; i++) {
                        drawBuffer.setBulb(i, aniSequence.color1, aniSequence.brightness);
                    }
                    for (local i = 0; i < math.rand() % drawBuffer.bulbs; i++) {
                        drawBuffer.setBulb(math.rand() % drawBuffer.bulbs, aniSequence.color2, aniSequence.brightness);
                    }            
                    break;
                    
                case "chaos":
                    for (local i = 0; i < drawBuffer.bulbs; i++) {
                        drawBuffer.setBulb(i, math.rand() % 0xFFF, aniSequence.brightness);
                    }
                    break;
                    
                case "fadein":
                    aniSequence.steps = aniSequence.steps <= 0 ? 1 : aniSequence.steps;
                    for (local i = 0; i < drawBuffer.bulbs; i++) {
                        drawBuffer.setBulb(i, aniSequence.color1, (aniFrame*aniSequence.steps) % 100);
                    }
                    break;
                    
                case "fadeout":
                    aniSequence.steps = aniSequence.steps <= 0 ? 1 : aniSequence.steps;
                    for (local i = 0; i < drawBuffer.bulbs; i++) {
                        drawBuffer.setBulb(i, aniSequence.color1, 100 - ((aniFrame*aniSequence.steps) % 100));
                    }
                    break;

                case "smooth":
                    local color1 = drawBuffer.toColorRgb(aniSequence.color1);
                    local color2 = drawBuffer.toColorRgb(aniSequence.color2);
                    if (aniSequence.steps <= 0) aniSequence.steps = 1;
                    if (aniSequence.frames == null) {
                        local distance = 0;
                        for (local i = 0; i < 3; i++) {
                            local d = math.abs(color1[0] - color2[0]);
                            if (d > distance) distance = d;
                        }
                        aniSequence.frames = (1.0 * distance / aniSequence.steps).tointeger() + 1;
                    }
                    
                    local step = aniFrame * aniSequence.steps;
                    local r  = (color1[0] + (color2[0] - color1[0]) * (step % (0xFF + 1)) / 0xFF).tointeger();
                    local g  = (color1[1] + (color2[1] - color1[1]) * (step % (0xFF + 1)) / 0xFF).tointeger();
                    local b  = (color1[2] + (color2[2] - color1[2]) * (step % (0xFF + 1)) / 0xFF).tointeger();

                    // server.log(format(" %02d [ %02X,%02X,%02X ]  +  [ %02X,%02X,%02X ] = [ %02X,%02X,%02X ]", aniFrame, color1[0], color1[1], color1[2], color2[0], color2[1], color2[2], r, g, b));
                    
                    local color = [r, g, b];                    
                    for (local i = 0; i < drawBuffer.bulbs; i++) {
                        drawBuffer.setBulb(i, color);
                    }
                    break;

            }

            // Render the buffer to the serial port
            renderBuffer();
        }
        
        aniFrame++;
        
        imp.wakeup(1.0 / updateBufferTimer, updateBuffer.bindenv(this));
    }
    

    // ........................................................................
    function renderBuffer() {
        // server.log("Render");
        local out = drawBuffer.render();
        hardware.spi.write(out);
        // Send it a second time to ensure the bits get the message.
        hardware.spi.write(out);
    }


}



// ----------------------------------------------------------------------------
class queue {
    
    items = null;
    
    // ........................................................................
    constructor() {
        items = [];
    }
    
    // ........................................................................
    function clear(dummy = null) {
        items.clear();
    }


    // ........................................................................
    function push(item) {
        items.push(item);
    }

    // ........................................................................
    function pop(sequence = null) {
        // Check if we have still got more time in the current animation
        if ("frames" in sequence && sequence.frames != null && --sequence.frames > 0) {
            // As you were
        } else {
            if ("frames" in sequence) sequence.frames = 0;
            
            if (items.len() > 0) {
                // Now remove that item
                local item = items[0];
                items.remove(0);
                item.new <- true;
                return item;
            }
        }
        
        if ("new" in sequence) sequence.new = false;
        
        return sequence;
    }    
}

    
// ----------------------------------------------------------------------------
class bulbBuffer {
    
    bulbs = 0;
    bulbDetails = null;

    colors = {
                "red":    [0xFF, 0x00, 0x00],
                "green":  [0x00, 0xFF, 0x00],
                "blue":   [0x00, 0x00, 0xFF],
                "aqua":   [0x00, 0xFF, 0xFF],
                "yellow": [0xFF, 0xFF, 0x00],
                "magenta":[0xFF, 0x00, 0xFF],
                "white":  [0xFF, 0xFF, 0xFF],
                "black":  [0x00, 0x00, 0x00],
                "pink":   [0xC0, 0x10, 0x80],                
                "purple": [0x80, 0x00, 0x80],
                "teal":   [0x00, 0x80, 0x80],
                "skyblue":[0x00, 0xBF, 0xFF],
                "orange": [0xFF, 0x60, 0x00]
    };
    

    // ........................................................................
    constructor(_bulbs) {
        
        bulbs = _bulbs;

        // Initialise the bulb array
        bulbDetails = [];
        for (local i = 0; i < bulbs; i++) {
            local newbulb = { "color": 0x00, "brightness": 0x00, "changed": true };
            bulbDetails.push(newbulb);
        }
    }
    
    
    // ........................................................................
    // drawBuffer.setBulb(i, [red, green, blue]);
    // drawBuffer.setBulb(i, 0xFFF);
    // drawBuffer.setBulb(i, "pink");
    function setBulb(position, color = 0x00, brightness = 100) {
        
        if (position >= 0 && position < bulbs) {
            color = toColorHex(color);
            brightness = (1.0 * brightness / 100.00 * 0xFF).tointeger();
            if (brightness > 0xFF) brightness = 0xFF;
            if (brightness < 0) brightness = 0x00;
            
            if ((bulbDetails[position].color != color) || (bulbDetails[position].brightness != brightness)) {
                bulbDetails[position].color = color;
                bulbDetails[position].brightness = brightness;
                bulbDetails[position].changed = true;
            }
        }
    }
    
    
    // ........................................................................
    function render() {
        
        local out = blob(28 * bulbs);

        for(local i=0; i<bulbs; i++) {
            
            if (bulbDetails[i].changed) {
                
                // 1 start bit
                out.writen(0x07, 'b');
            
                // 6 bit address
                out.writeblob(makebits(i, 6));
                
                // 8 bit brightness
                out.writeblob(makebits(bulbDetails[i].brightness, 8));
                
                // 12 bit color
                out.writeblob(makebits(bulbDetails[i].color, 12));
                
                // Back to zero
                out.writen(0x00, 'b');
                
                // Mark it as unchanged
                bulbDetails[i].changed = false;
            }
        }
        
        // Crop the blob at the end.
        out.resize(out.tell());
        
        return out;        
    }
    

    // ........................................................................
    function makebits(value, numbits) {
        local out = blob(numbits);
        for (local a = 0; a < numbits; a++) {
            if (value&(1<<(numbits-a-1))){
                out.writen(0x03, 'b');
            }else{
                out.writen(0x1f, 'b');
            }
        }
        return out;
    }


    // ........................................................................
    function toColorHex(color) {
        local newcolor = null;
        if (typeof color == "string") {
            if (color in colors) color = colors[color];
            else newcolor = 0xFFF;
        } 
        if (typeof color == "array") {
            newcolor = ((0xF0 & color[2]) << 4) | (0xF0 & color[1]) | ((0xF0 & color[0]) >> 4);
        } 
        if (typeof color == "integer") {
            newcolor = color;
        }
        
        return newcolor;
    }
    
    
    // ........................................................................
    function toColorRgb(color) {
        local newcolor = null;
        if (typeof color == "string") {
            if (color in colors) newcolor = colors[color];
            else newcolor = [0xFF, 0xFF, 0xFF];
        } 
        if (typeof color == "integer") {
            newcolor = [];
            newcolor.push((color >> 8 & 0x00F) << 4);
            newcolor.push((color >> 4 & 0x00F) << 4);
            newcolor.push((color >> 0 & 0x00F) << 4);
        } 
        
        return newcolor;
    }
    
}


// ----------------------------------------------------------------------------
c <- colorEffects();
server.log("Agent ready!");
