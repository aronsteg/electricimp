<?php

$debug = isset($_REQUEST['debug']);
$height = isset($_REQUEST['height']) ? $_REQUEST['height'] : 16;
$width = isset($_REQUEST['width']) ? $_REQUEST['width'] : 16;
$url = "http://www.imagessharedstorage.com/files/g4xpdJ01QAokc04qH252QQk4li15NYAtSxZu4NlLOPtoElVLeOE/Seven_segment_display-animated1.gif";
$url = isset($_REQUEST['url']) ? $_REQUEST['url'] : $url;
$hash = md5($url);

if (!$debug) {
	header("Content-type: application/octet-stream");
	header('Content-Disposition: attachment; filename="image.rgb"');
}


// Load the cache if its not already loaded
if (!is_readable($hash)) {
	mkdir($hash);

	// Create a new imagick object and read in the image
	file_put_contents("$hash/frame_XXX_o.gif", file_get_contents($url));
	$img = new Imagick("$hash/frame_XXX_o.gif");

	// Flatten the frame layers
	$img = $img->coalesceImages();

	// Resize each frame and store the original and thumbnail frames
	$f = 0;
	foreach ($img as $frame) {
		$frame->writeImage(sprintf("$hash/frame_%03d_o.gif", $f));
		$frame->thumbnailImage($width, $height);
		$frame->writeImage(sprintf("$hash/frame_%03d_t.gif", $f));
		$f++;
	}
}


// Now out put the image frames
if ($debug) {
	$img = new Imagick("$hash/frame_XXX_o.gif");
	echo "<html><body style='background-color: #000000'>";
	echo "<img src='$hash/frame_XXX_o.gif' width='230' /><br/><hr/>\n";
	for ($f = 0; true; $f++) {
		$fn_o = sprintf("$hash/frame_%03d_o.gif", $f);
		$fn_t = sprintf("$hash/frame_%03d_t.gif", $f);
		if (is_readable($fn_o) && is_readable($fn_t)) {
			echo "<img src='$fn_o' width='230' /><br/>\n";
			echo "<img src='$fn_t' width='230' /><br/>\n";
			echo "<pre>";
			$img = new Imagick($fn_t);
			$pixels = $img->exportImagePixels(0, 0, $width, $height, "ARGB", Imagick::PIXEL_CHAR);
			for ($i = $p = 0; $i < count($pixels); $i += 4, $p++) {
				$y = floor($p / $height);
				$x = $p % $width;
				$pixel = (($pixels[$i+1] & 0xFF) << 16) | (($pixels[$i+2] & 0xFF) << 8) | $pixels[$i+3] & 0xFF;
				$line = "";
				if ($x == $width-1) {
					$line = "\n";
				}
				if ($pixels[$i] == 0) {
					printf("<font color='#%06X'>_</font>%s", $pixel, $line);
				} else {
					printf("<font color='#%06X'>@</font>%s", $pixel, $line);
				}
			}
			echo "</pre><br/><hr/>\n";
		} else {
			break;
		}
	}
} else {
	$bin = "";
	for ($f = 0; true; $f++) {
		$fn_o = sprintf("$hash/frame_%03d_o.gif", $f);
		$fn_t = sprintf("$hash/frame_%03d_t.gif", $f);
		if (is_readable($fn_o) && is_readable($fn_t)) {
			$img = new Imagick($fn_t);
			$pixels = $img->exportImagePixels(0, 0, $width, $height, "ARGB", Imagick::PIXEL_CHAR);
			for ($i = 0; $i < count($pixels); $i += 4) {
				if ($pixels[$i] == 0) {
					// This is transparent.
					$bin .= pack("CCC", 0x0, 0x0, 0x0);
				} else {
					$bin .= pack("CCC", $pixels[$i+1] & 0xFF, $pixels[$i+2] & 0xFF, $pixels[$i+3] & 0xFF);
				}
			}
		} else {
			break;
		}
	}
	echo pack("NNN", $f, $width, $height) . $bin;
}
