
server.log("Agent URL is " + http.agenturl());
partner <- (http.agenturl() == "http://agent.electricimp.com/Lb12gH3kHI8Z") ? "http://agent.electricimp.com/2KSCXnAyCYTs" : "http://agent.electricimp.com/Lb12gH3kHI8Z";

device.on("farther", function (proximity) {
   http.post(partner, {}, http.urlencode({"command": "farther", "proximity": proximity})).sendasync(function (res) {})
});
device.on("closer", function (proximity) {
   http.post(partner, {}, http.urlencode({"command": "closer", "proximity": proximity})).sendasync(function (res) {})
});

http.onrequest(function (req, res) {
    local post = http.urldecode(req.body);
    device.send(post.command, post.proximity);
    res.send(200);
});
