// ========================================================================================
// Sample application: ColorEffects
// Animates the GE ColorEffects christmas lights.
//
// Valid commands: pushQueue, clearQueue
// Valid params for pushQueue: animation (string), color1/2 (string|integer|[r,g,b]), frames (int), brightness (float%), 
// Valid animations: walk, chaos, twinkle, fade, smooth, fixed


device.on("status", function (status) {

    server.log("Device ready!");
    
    // The imp has just come online
    device.send("pushQueue", { "animation": "chaos", "frames": 200, "speed": 20, "brightness": 100 });
    device.send("pushQueue", { "animation": "fadein", "color1": "red", "color2": "blue", "frames": 20, "steps": 5 });
    device.send("pushQueue", { "animation": "fadeout", "color1": "red", "color2": "blue", "frames": 20, "steps": 5 });
    device.send("pushQueue", { "animation": "walk", "color1": "red", "color2": "green", "steps": 6, "frames": 20, "speed": 2 });
    device.send("pushQueue", { "animation": "twinkle", "color1": "blue", "color2": "white", "frames": 20, "brightness": 100.0 });
    device.send("pushQueue", { "animation": "smooth", "color1": "black", "color2": "orange", "brightness": 100.0, "steps": 0x05, "speed": 10 });
    device.send("pushQueue", { "animation": "smooth", "color1": "orange", "color2": "red", "brightness": 100.0, "steps":  0x05, "speed": 10 });
    device.send("pushQueue", { "animation": "smooth", "color1": "red", "color2": "blue", "brightness": 100.0, "steps":  0x05, "speed": 10 });
    device.send("pushQueue", { "animation": "smooth", "color1": "blue", "color2": "green", "brightness": 100.0, "steps":  0x05, "speed": 10 });
    device.send("pushQueue", { "animation": "smooth", "color1": "green", "color2": "yellow", "brightness": 100.0, "steps":  0x05, "speed": 10 });
    device.send("pushQueue", { "animation": "smooth", "color1": "yellow", "color2": "black", "brightness": 100.0, "steps":  0x05, "speed": 10 });
    device.send("pushQueue", { "animation": "fixed", "color1": "black" });
    
});
