
// ------------------------------------------------------------------------
// Takes a string, renders it and sends it to the device for display
text <- null;
function displayText(_text) {
    device.send("text", _text);    
    text = _text;
}


// ------------------------------------------------------------------------
// Takes a name of an animation and sends it to the device for rendering
animation <- null;
seed <- "";
function displayAnimation(_animation, _seed=null) {
    if (_animation != "none") {
        device.send("clear", null);        
        url = null;
    }
    device.send("animate", {"animation":_animation, "seed":_seed});
    animation = _animation;
    seed = _seed;
    
    // Send the text overlay
    if (text) displayText(text);
    
    
}


// ------------------------------------------------------------------------
// Takes a url, sends it to the server to be converted. Reads the converted image
// and sends it on to the device as a blob of rgb bitmap frames.
url <- null;
function displayImage(_url) {
    local convertUrl = "http://buzz-03.smsd.com.au/leddisplay/read.php";
    if (_url) animation = "none";
    
    server.log("Loading url: " + _url);
    
    local params = {"url": _url};
    local res = http.get(convertUrl + "?" + http.urlencode(params)).sendasync(function(res) {
        if (res.statuscode == 200) { // "OK"
        
            server.log("Finished loading url: " + _url + " (" + res.body.len() + ")");
            if (res.body.len() == 0) {
                server.log("The image returned from the conversion is blank. Rejecting it.")
                return;
            } else if (res.body.len() > 24000) {
                server.log("This image is too big. Rejecting it.")
                return;
            }
            
            try {
                // Check the file is valid
                if (res.body.len() < 12) {
                    server.log("This image is really too small. Rejecting it.")
                    return;
                }
                
                // Read the headers out of the file
                local i = 0;
                local f = res.body[i++] & 0xFF << 24 | res.body[i++] & 0xFF << 16 | res.body[i++] & 0xFF << 8 | res.body[i++] & 0xFF;
                local w = res.body[i++] & 0xFF << 24 | res.body[i++] & 0xFF << 16 | res.body[i++] & 0xFF << 8 | res.body[i++] & 0xFF;
                local h = res.body[i++] & 0xFF << 24 | res.body[i++] & 0xFF << 16 | res.body[i++] & 0xFF << 8 | res.body[i++] & 0xFF;

                // Double check the size again
                if (res.body.len() < 12+(f*w*h*3)) {
                    server.log("This image is too small. Rejecting it.")
                    return;
                } else if (res.body.len() > 12+(f*w*h*3)) {
                    server.log("This image is too big but I am going to truncate it.")
                }
                
                // Clear the server
                device.send("clear", null);
                
                // Put each frame in a blob 
                for (local ff = 0; ff < f; ff++) {
                    local frame = blob(w*h*3);
                    for (local ii = 0; ii < w*h*3; ii++) {
                        frame.writen(res.body[i++], 'b');
                    }
                    device.send("rgb", frame);
                }
                
                // Send the text overlay
                if (text) displayText(text);
                
                url = _url;
            } catch (e) {
                server.log(e);
            }
            
        } else {
            
            server.log("Error " + res.statuscode + " loading " + _url);
            
        }
    });
}


// ------------------------------------------------------------------------
// Creates a web page for letting the user control the display. This is served off the agent URL.
// Available functions: upload an image (url), display text, clear screen.
http.onrequest(function (request,response) {
    local newurl = null, newtext = null, newanimation = null;
    if (request.method == "GET") {
        if ("url" in request.query) newurl = request.query.url;
        if ("text" in request.query) newtext = request.query.text;
        if ("animation" in request.query) newanimation = request.query.animation;
        if ("seed" in request.query) seed = request.query.seed;
    } else if (request.method == "POST") {
        local post = http.urldecode(request.body);
        if ("url" in post) newurl = post.url;
        if ("text" in post) newtext = post.text;
        if ("animation" in post) newanimation = post.animation;
        if ("seed" in post) seed = post.seed;
    }

    if (newurl != null) {
        displayImage(newurl);
    } else {
        newurl = url ? url : "";
    }
    if (newtext != null) {
        displayText(newtext);
    } else {
        newtext = text ? text : "";
    }
    if (newanimation != null) {
        displayAnimation(newanimation, seed);
    } else {
        newanimation = animation ? animation : "";
    }

    local html = "";
    html += "<html><head><meta name='viewport' content='width=device-width'></head><body>";
    html += "Set the animated image on the LED display.<br/>";
    html += "<form method='post'>";
    html += "<img height=160 width=160 src='" + newurl + "' /><br/>";
    
    local images = ["http://www.shoesuperstore.com.au/media/icons/Black.gif",
                    "http://www.mariowiki.com/images/e/e5/Animated_Yakumario.gif",
                    "http://fanart-central.net/avatars/67977.gif",
                    "http://i925.photobucket.com/albums/ad98/MarioBabyLuigi/Dinothingparty.gif",
                    "http://www.imagessharedstorage.com/files/g4xpdJ01QAokc04qH252QQk4li15NYAtSxZu4NlLOPtoElVLeOE/Seven_segment_display-animated1.gif",
                    "http://www.purchase.org/public/default/frontend/standard/images/subscribe-loader.gif",
                    "http://ak.imgfarm.com/images/cursormania/files/22/11246a.gif",
                    "http://www.gifs.net/Animation11/Sports/Track_and_Field/Child_runs_4.gif",
                    "http://moniabenini.com/mod/monia/img/running_dog.gif",
                    "http://www.123cursors.com/freecursors/9082.gif",
                    "http://www.freeonlinestuffs.com/images/cursors/flag-cursors.gif",
                    "https://si0.twimg.com/profile_images/513097239/JenStarkFavicon.gif",
                    "http://www.freewebs.com/al-smith/_traffic_light_p.gif",
                    "http://www.ljplus.ru/img4/p/i/pinwizz/runtr13.gif",
                    "http://zeldapower.com/forum/images/misc/fire.gif",
                    "http://i585.photobucket.com/albums/ss300/mndless/forum/cartman1.gif",
                    "http://files.backyardchickens.com/img/smilies/D.gif",
                    "http://downloads.totallyfreecursors.com/thumbnails/tail.gif",
                    "http://cdn.fupa.com/small/dupworld.gif",
                    "http://beta.scratch.mit.edu/static/site/galleries/thumbnails/15/5870.png",
                    "http://24.media.tumblr.com/f1fe83709fa1e9c265d755ca9383a5ce/tumblr_midbyoylyd1r6thx9o1_500.gif",
                    "http://forum.thefunmouse.com/images/smilies/EatCheese.gif",
                    "http://songbird61.mypldi.net/music/aninotes21.gif",
                    "http://www.unexplained-mysteries.com/forum/uploads/av-128070.gif",
                    "http://i623.photobucket.com/albums/tt313/Webswimmer12/dance_banana_not_1_.gif",
                    "http://buzz-03.smsd.com.au/leddisplay/tom.jpg",
                    "http://buzz-03.smsd.com.au/leddisplay/kevin.jpg"
                    ];
    for (local i = 0; i < images.len(); i++) {
        html += "<a href='?url=" + images[i] + "'><img src='" + images[i] + "' width=32 height=32 /></a> ";
    }
    
    html += "<br/>\n";
    html += "Manual: <input name='url' size='100' value='" + newurl + "'><br/>";
    html += "<input type='submit'><br/><br/>";
    html += "</form>";
    
    html += "<form method='post'>";
    html += "Animation: <select name='animation'>";
    local animations = ["none", "clear", "life", "randomwalk", "matrix"];
    for (local i = 0; i < animations.len(); i++) {
        local selected = (animations[i] == newanimation) ? "selected" : "";
        html += "<option value='" + animations[i] + "' " + selected + ">" + animations[i] + "</option>";
    }
    html += "</select> ";
    html += "Seed: <input name='seed' size='100' value='" + seed + "'><br/>";
    html += "<input type='submit'><br/><br/>";
    html += "</form>";
    
    html += "<form method='post'>";
    html += "Text overlay: <input name='text' size='100' value='" + newtext + "'><br/>";
    html += "<input type='submit'><br/><br/>";
    html += "</form>";
    
    html += "</body>";
    response.send(200, html);

});


// ------------------------------------------------------------------------
// This code executes when the device boots.
started <- false;
device.on("status", function (dummy=null) {
    // if (!started) displayImage("http://www.mariowiki.com/images/e/e5/Animated_Yakumario.gif");
    // if (!started) displayText("@electricimp");
    started = true;
});


// ------------------------------------------------------------------------
// This code is executed when the agent boots
server.log("Agent started! URL is " + http.agenturl());
device.send("clear", null);
