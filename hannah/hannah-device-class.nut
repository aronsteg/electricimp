// IO Expander Class for SX1509
class IoExpander
{
    i2cPort = null;
    i2cAddress = null;
    irqCallbacks = array(16);
    i2cExpanderAddress = 0x7C;
 
    constructor(port, address)
    {
        if(port == I2C_12)
        {
            // Configure I2C bus on pins 1 & 2
            i2cPort = hardware.i2c12;
        }
        else if(port == I2C_89)
        {
            // Configure I2C bus on pins 8 & 9
            i2cPort = hardware.i2c89;
        }
        else
        {
            server.log("Invalid I2C port specified.");
            return;
        }
        i2cPort.configure(CLOCK_SPEED_100_KHZ);
 
        i2cAddress = address << 1;
        hardware.pin1.configure(DIGITAL_IN, getIRQSources.bindenv(this));

    }
 
    // Read a byte or bytes
    function read(register, bytes = 1)
    {
        local data = i2cPort.read(i2cAddress, format("%c", register), bytes);
        if(data == null)
        {
            server.log(format("I2C Read Failure of %d bytes from device 0x%02X at register 0x%02X", bytes, i2cAddress >> 1, register));
            return null;
        }
        
        return (bytes == 1) ? data[0] : data;
    }
 
    // Write a byte
    function write(register, ...)
    {
        local data = format("%c", register);
        for (local i = 0; i < vargv.len(); i++) {
            data += format("%c", vargv[i]);
        }
        local success = i2cPort.write(i2cAddress, data);
        
        if (success != 0) {
            server.log(format("I2C Write Failure of %d bytes to device 0x%02X at register 0x%02X", data.len()-1, i2cAddress >> 1, register));        
        }
    }
 
    // Write a bit to a register
    function writeBit(register, bitn, level)
    {
        local value = read(register);
        value = (level == 0)?(value & ~(1<<bitn)):(value | (1<<bitn));        
        write(register, value);
    }
 
    // Write a masked bit pattern
    function writeMasked(register, data, mask)
    {
       local value = read(register);
       value = (value & ~mask) | (data & mask);
       write(register, value);
    }
 
    // Set a GPIO level
    function setPin(gpio, level)
    {
        writeBit(gpio>=8?0x10:0x11, gpio&7, level?1:0);
    }
 
    // Set a GPIO direction
    function setDir(gpio, output)
    {
        writeBit(gpio>=8?0x0e:0x0f, gpio&7, output?0:1);
    }
 
    // Set a GPIO internal pull up
    function setPullUp(gpio, enable)
    {
        writeBit(gpio>=8?0x06:0x07, gpio&7, enable);
    }
 
    // Set GPIO interrupt mask
    function setIrqMask(gpio, enable)
    {
        writeBit(gpio>=8?0x12:0x13, gpio&7, enable);
    }
 
    // Set GPIO interrupt edges
    function setIrqEdges(gpio, rising, falling)
    {
        local addr = 0x17 - (gpio>>2);
        local mask = 0x03 << ((gpio&3)<<1);
        local data = (2*falling + rising) << ((gpio&3)<<1);    
        writeMasked(addr, data, mask);
    }
 
    // Clear an interrupt
    function clearIrq(gpio)
    {
        writeBit(gpio>=8?0x18:0x19, gpio&7, 1);
    }
 
     function setIRQCallBack(pin, func){
        irqCallbacks[pin] = func;
    }
    
    function clearIRQCallBack(pin){
           irqCallbacks[pin] = null;
    }
    
    function getIRQSources(){
        local sourceB = read(0x18);
        local sourceA = read(0x19);
        local irqSources = array(16);
        
        local j = 0;
        for(local z=1; z < 256; z = z<<1){
            irqSources[j] = ((sourceA & z) == z);
            irqSources[j+8] = ((sourceB & z) == z);
            j++;
        }
        
        for(local pin=0; pin < 16; pin++){
            if(irqSources[pin]){
                irqCallbacks[pin]();
                clearIrq(pin);
            }
        }
        
       return irqSources;    //Array of the IO pins and who has active interrupts
    }

    // Get a GPIO input pin level
    function getPin(gpio)
    {
        return (read(gpio>=8?0x10:0x11)&(1<<(gpio&7)))?1:0;
    }
}
 
// RGB LED Class
class RgbLed extends IoExpander
{
    // IO Pin assignments
    pinR = null;
    pinG = null;
    pinB = null;
 
    constructor(port, address, r, g, b)
    {
        base.constructor(port, address);
 
        // Save pin assignments
        pinR = r;
        pinG = g;
        pinB = b;
 
        // Disable pin input buffers
        writeBit(pinR>7?0x00:0x01, pinR>7?(pinR-7):pinR, 1);
        writeBit(pinG>7?0x00:0x01, pinG>7?(pinG-7):pinG, 1);
        writeBit(pinB>7?0x00:0x01, pinB>7?(pinB-7):pinB, 1);
 
        // Set pins as outputs
        writeBit(pinR>7?0x0E:0x0F, pinR>7?(pinR-7):pinR, 0);
        writeBit(pinG>7?0x0E:0x0F, pinG>7?(pinG-7):pinG, 0);
        writeBit(pinB>7?0x0E:0x0F, pinB>7?(pinB-7):pinB, 0);
 
        // Set pins open drain
        writeBit(pinR>7?0x0A:0x0B, pinR>7?(pinR-7):pinR, 1);
        writeBit(pinG>7?0x0A:0x0B, pinG>7?(pinG-7):pinG, 1);
        writeBit(pinB>7?0x0A:0x0B, pinB>7?(pinB-7):pinB, 1);
 
        // Enable LED drive
        writeBit(pinR>7?0x20:0x21, pinR>7?(pinR-7):pinR, 1);
        writeBit(pinG>7?0x20:0x21, pinG>7?(pinG-7):pinG, 1);
        writeBit(pinB>7?0x20:0x21, pinB>7?(pinB-7):pinB, 1);
 
        // Set to use internal 2MHz clock, linear fading
        write(0x1e, 0x50);
        write(0x1f, 0x10);
 
        // Initialise as inactive
        setLevels(0, 0, 0);
        setPin(pinR, 0);
        setPin(pinG, 0);
        setPin(pinB, 0);
    }
 
    // Set LED enabled state
    function setLed(r, g, b)
    {
        if(r != null) writeBit(pinR>7?0x20:0x21, pinR&7, r);
        if(g != null) writeBit(pinG>7?0x20:0x21, pinG&7, g);
        if(b != null) writeBit(pinB>7?0x20:0x21, pinB&7, b);
    }
 
    // Set red, green and blue intensity levels
    function setLevels(r, g, b)
    {
        if(r != null) write(pinR<4?0x2A+pinR*3:0x36+(pinR-4)*5, r);
        if(g != null) write(pinG<4?0x2A+pinG*3:0x36+(pinG-4)*5, g);
        if(b != null) write(pinB<4?0x2A+pinB*3:0x36+(pinB-4)*5, b);
    }
}


// Class for the RGB light sensor on the Hannah board
enum rgbdcs { red = 0, green = 1, blue = 2, clear = 3 };
class RgbSensor extends IoExpander {

    static rgbdcsCapRed = 0x06; // -? 0x09 (x4) for Red, Green, Blue & Clear
    static rgbdcsIntRedLo = 0x0A; // -> 0x11 (x4) for Red, Green, Blue & Clear 
    static rgbdcsIntRedHi = 0x0B;
    static rgbdcsDatRedLo = 0x40; // -> 0x47 (x4) for Red, Green, Blue & Clear 
    static rgbdcsDatRedHi = 0x41;
    static rgbdcsOffsetRed = 0x48; // -> 0x4B (x4) for Red, Green, Blue & Clear
    

    // Constructor
    constructor (port, address) {

        // Call base constructor
        base.constructor (port, address);
        
        // Start by forcing a RESET on the ADJD-S311-CR999 (through RESET from SX1509 on Hannah)
        // consecutive 0x12 and 0x34 to RegReset (0x7C)
        i2cPort.write(i2cExpanderAddress, "\x7C\x12"); 
        i2cPort.write(i2cExpanderAddress, "\x7C\x34"); 
        
        // Now cycle through colors from RED thru CLEAR
        local rgbdcsCapas = {}, sensorVal = null;
        for (local color = rgbdcs.red; color <= rgbdcs.clear; color++ ) { 
            
            // Now cycle thru all Cap values 0x00 -> 0x0F
            for (local capas = 0; capas <= 0x0F; capas++) {
                
                // Write the Cap register (for a color) with the current Cap value
                write(rgbdcsCapRed + color, capas);
     
                // Write the Intensity register with my default value (2048 or 0x0800)
                write(rgbdcsIntRedLo + (color * 2), 0x00, 0x08);
                write(0, 1); // Tell ConTRoL reg we want to read sensor (0x01)
                imp.sleep(0.200); // Wait for conversion to finish
                
                local status = read(0); // Check the control register to be ready 0x00
                if (status == null) {
                    server.log("RgbSensor - Control register read error");
                    break;
                } else {
        
                    // Now try to read the sensor
                    local data = read(rgbdcsDatRedLo + (color * 2), 2); 
                    if (data == null) {
                        server.log("RgbSensor - Data Register read error");
                        break;
                    } else {
            
                        sensorVal = ((data[1] & 0x0F) << 4) + data[0];
                        
                        // WOW, we are below the threshold
                        if (sensorVal <= 1000) { 
                            rgbdcsCapas[color] <- capas;
                            capas = 0x0F;
                        }
                    }
                }
            }
        }
     
        if (3 in rgbdcsCapas) {
            // server.log(format("RgbSensor - Capacitors: R=%d G=%d B=%d C=%d", rgbdcsCapas[0], rgbdcsCapas[1], rgbdcsCapas[2], rgbdcsCapas[3]));
        }
    }
    

    function get() { 
    
        // Start a conversion and wait for it to finish
        write(0, 1);
    	imp.sleep(0.050); 
        
    	local data = read(0x0);
    	if (data == null) { 
    		server.log("RgbSensor - Error during read status request"); 
    		return null; 
    	}
    
        // Read actual raw LO and HI values of each color
        local results = {};
        for (local color = rgbdcs.red; color <= rgbdcs.clear; color++ ) { 
        	local byteLo = read(rgbdcsDatRedLo+color); 
        	local byteHi = read(rgbdcsDatRedHi+color);
    	    results[color] <- (((byteHi & 0x0F) << 4) + byteLo); 
        }
        
        return results;
    }

}




// Class for the potentiometer on the Hannah board
class Potentiometer extends IoExpander {

    // Constructor
    constructor (port, address) {

        // Call base constructor
        base.constructor (port, address);
        
        // Enable Potentiometer
        setPin(8, 0);
        setDir(8, 1);
        hardware.pin2.configure(ANALOG_IN);
        
    }
    
    // Gets the current value
    function get () {
        return hardware.pin2.read() / 65535.0;
    }
}

 
// Class for the button on the Hannah board
class Button extends IoExpander {

    // IO Pin assignment
    pin = null;
    irq = null;
 
    // Callback function for interrupt
    callBack = null;
 
    // Constructor
    constructor (port, address, btnPin, call) {

        // Call base constructor
        base.constructor (port, address);
        
        // Enable button
        // Save assignments
        pin = btnPin;
        callBack = call;
 
        // Set event handler for irq
        setIRQCallBack(btnPin, irqHandler.bindenv(this))
 
        // Configure pin as input, irq on both edges
        setDir(pin, 0);
        setPullUp(pin, 1);
        setIrqMask(pin, 0);
        setIrqEdges(pin, 1, 1);
        
    }

    function irqHandler()
    {
        // Get the pin state
        local state = getPin(pin)?0:1;

        // server.log(format("Push Button %d = %d", pin, state));
        if (callBack != null) callBack(state)  
        
        // Clear the interrupt
        clearIrq(pin);
    }
    
    function readState()
    {
        local state = getPin(pin)?0:1;
        return state;
    }
}


// Accelerometer Class
class Accelerometer extends IoExpander // clearly, this is not really an IOExpander, we're just nicking it's read() function
{
    pin = 3;
    
    constructor(port, address)
    {
        base.constructor(port, address);
        
        // write(0x20, 0x47); // Power up with all three axes enabled
        write(0x20, 0x41); // Bring device out of power-down, X axis enabled
        write(0x22, 0x10); // Both interrupt lines are active
        write(0x30, 0x02); // Wake-up config when x axis exceeds threshold
        write(0x32, 0x01); // Wake-up threshold
        write(0x33, 0x01); // Signal for ages on triggering
        write(0x14, 0xff);
        write(0x15, 0xff);
        write(0x16, 0xff);
        write(0x17, 0xff); // Edge-sensitivity: all
        
        setDir(pin, 0);
        setPullUp(pin, 1);
        setIrqMask(pin, 0);
        setIrqEdges(pin, 1, 1);
        
        setIRQCallBack(pin, irqHandler.bindenv(this))
    }
    
    function irqHandler() {
        server.log("IRQ")
        
        // Clear the interrupt
        clearIrq(pin);
    }
    
    function getX() {
        return (read(0x29) + 127) % 256 - 128;
    }
    function getY() {
        return (read(0x2b) + 127) % 256 - 128;
    }
    function getZ() {
        return (read(0x2d) + 127) % 256 - 128;
    }
}
 


// -----------------------------------------------------------------------------

// Register with the server
imp.configure("Hannah", [], []);
server.log("Device is Hannah with impee_id " + hardware.getimpeeid() + " and mac = " + imp.getmacaddress() );

// Construct an object for direct access to the expander
ioexpander <- IoExpander(I2C_89, 0x3E);

// Construct an LED and enable the LED outputs 
led <- RgbLed(I2C_89, 0x3E, 7, 5, 6);
led.setLed(1, 1, 1);

// Wake up the RGB light sensor
ioexpander.setDir(9, 1);
ioexpander.setPin(9, 0); 

// Construct an RGB light sensor
rgbsensor <- RgbSensor(I2C_89, 0x74);

// Construct a potentiometer and hook it to the LEDs
pot <- Potentiometer(I2C_89, 0x3E);
function checkPot() {
    imp.wakeup(0.2, checkPot);
    if (btn1_state == 0 && btn2_state == 1) {
        local potval = pot.get();
        led.setLevels(0, (potval*255).tointeger(), 0)
    }
}
imp.wakeup(0, checkPot);

// Setup the pushbuttons with event handlers
btn1_state <- 0;
btn1 <- Button(I2C_89, 0x3E, 0, function(st) { 
    btn1_state = st ? 1 : 0;
    if (st) {
        // server.log("Button 1 is now " + (st ? "on" : "off"))    
        local colors = rgbsensor.get();
        led.setLevels(colors[0], colors[1], colors[2])
    }
});

btn2_state <- 0;
btn2 <- Button(I2C_89, 0x3E, 1, function(st) { 
    btn2_state = st ? 1 : 0;
    if (!st) {
        // server.log("Button 2 is now " + (st ? "on" : "off"))    
        led.setLevels(0, 0, 0);
    }
});

// Setup the accelerometer and read
/*
local acc = Accelerometer(I2C_89, 0x1C);
function checkAccel() {
    imp.wakeup(0.5, checkAccel);
    // server.log(format("[%d, %d, %d]", acc.getX(), acc.getY(), acc.getZ() ));
    // server.log(acc.read(0x11) + "," + acc.getPin(3) + "+" + (acc.read(0x31) & 0x2A));
    
    local dim = pot.get();
    led.setLevels((acc.getX()+128)*dim, (acc.getY()+128)*dim, (acc.getZ()+128)*dim);
}
imp.wakeup(0, checkAccel);
*/


