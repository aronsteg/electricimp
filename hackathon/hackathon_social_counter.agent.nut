

// This polls once a minute to both twitter and facebook to get the latest data
fb_likes <- 0;
tw_followers <- 0;
function update(data = null) {
  
  local new_fb_likes = 0;
  local new_tw_followers = 0;
  
  try {
    local url = "https://graph.facebook.com/?ids=http://www.facebook.com/electricimp";
    local response = http.get(url).sendsync();
    local json = http.jsondecode(response.body);
    new_fb_likes = json["http://www.facebook.com/electricimp"]["likes"];
  } catch (e) {
    server.log("Facebook failed: " + e)
  }
  
  try {
    
    // Pick up a token
    local url = "https://api.twitter.com/oauth2/token";
    local consumerkey = "iAUMAwAzZN27foqNFghLw";
    local consumersecret = "gcKn3vbzOHo1ETBnh2mOMKS0Q8NTvJv1YLLyEQ5qMRk";
    local bearertokencreds = http.base64encode(consumerkey + ":" + consumersecret);
    local auth = "Basic " + bearertokencreds;
    local headers = {Authorization = auth, "Content-Type":"application/x-www-form-urlencoded;charset=UTF-8"};
    local response = http.post(url, headers, "grant_type=client_credentials").sendsync();
    local json = http.jsondecode(response.body);
    local bearertoken = json["access_token"];
    
    // Request the data
    local url = "https://api.twitter.com/1.1/users/show.json?screen_name=electricimp";
    local auth = "Bearer " + bearertoken;
    local headers = {Authorization = auth};
    local response = http.get(url, headers).sendsync();
    local json = http.jsondecode(response.body);
    new_tw_followers = json["followers_count"];
  } catch (e) {
    server.log("Twitter failed: " + e)
  }
  
  // If we have more followers then hit the cowbell
  if ((new_fb_likes > fb_likes && fb_likes > 0) || (new_tw_followers > tw_followers && tw_followers > 0)) {
    local url = "https://agent.electricimp.com/9Wg_jragoqDu";
    http.get(url, {}).sendsync();
    server.log("Gong!")
  }
  local new_likes = fb_likes > 0 ? new_fb_likes - fb_likes : 0;
  local new_followers = tw_followers > 0 ? new_tw_followers - tw_followers : 0;
  
  if (new_fb_likes > 0) fb_likes = new_fb_likes;
  if (new_tw_followers > 0) tw_followers = new_tw_followers;
  
  // We have sufficient data, now send it to the device.
  local text = "\n ";
  local new_likes_str = new_likes == 0 ? "" : format(" (%d new)", new_likes);
  local new_followers_str = new_followers == 0 ? "" : format(" (%d new)", new_followers);
  text = format("%s Facebook likes: %d%s, ", text, fb_likes, new_likes_str);
  text = format("%s Twitter followers: %d%s.", text, tw_followers, new_followers_str);
  
  device.send("set_text", text);
  
}


// When the device comes online it sends a "Ready" message. Start (or restart) the regular polling
device.on("ready", update);

