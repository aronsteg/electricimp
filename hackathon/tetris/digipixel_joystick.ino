#include <DigiPixel.h>

DigiPixel digiPixel(5,2,6,4,3);  // LED Latch/Button Shift !load pin, LED/Button clock pin, LED Data Pin, LED Output Enable pin, Buttons data pin)
unsigned long last_loop = 0;

char PIECES[4][4][7][5] = 
{ { { "    ",   "    ",   "    ",   "    ",   "    ",   "    ",   "    " },
    { "    ",   "B   ",   "  O ",   " YY ",   " GG ",   " P  ",   "RR  " },
    { "bbbb",   "BBB ",   "OOO ",   " YY ",   "GG  ",   "PPP ",   " RR " },
    { "    ",   "    ",   "    ",   "    ",   "    ",   "    ",   "    " } },
  { { " b  ",   "    ",   "    ",   "    ",   "    ",   "    ",   "  R " },
    { " b  ",   " B  ",   "OO  ",   " YY ",   " G  ",   " P  ",   " RR " },
    { " b  ",   " B  ",   " O  ",   " YY ",   " GG ",   " PP ",   " R  " },
    { " b  ",   "BB  ",   " O  ",   "    ",   "  G ",   " P  ",   "    " } },
  { { "    ",   "    ",   "    ",   "    ",   "    ",   "    ",   "    " },
    { "    ",   "    ",   "    ",   " YY ",   " GG ",   "    ",   "RR  " },
    { "bbbb",   "BBB ",   "OOO ",   " YY ",   "GG  ",   "PPP ",   " RR " },
    { "    ",   "  B ",   "O   ",   "    ",   "    ",   " P  ",   "    " } },
  { { " b  ",   "    ",   "    ",   "    ",   "    ",   "    ",   "  R " },
    { " b  ",   " BB ",   " O  ",   " YY ",   " G  ",   " P  ",   " RR " },
    { " b  ",   " B  ",   " O  ",   " YY ",   " GG ",   "PP  ",   " R  " },
    { " b  ",   " B  ",   " OO ",   "    ",   "  G ",   " P  ",   "    " } } };


void setup(void) {
  Serial.begin(115200);
}

void checkButtons(void) {
  char buttonMap[7] = "lrudab";
  if (digiPixel.buttonLeftPressed == true) buttonMap[0] = 'L';
  if (digiPixel.buttonRightPressed == true) buttonMap[1] = 'R';
  if (digiPixel.buttonUpPressed == true) buttonMap[2] = 'U';
  if (digiPixel.buttonDownPressed == true) buttonMap[3] = 'D';
  if (digiPixel.buttonAPressed == true) buttonMap[4] = 'A';
  if (digiPixel.buttonBPressed == true) buttonMap[5] = 'B';
  Serial.println(buttonMap);
}

int checkSerial(char *buffer) {
  // Load one line of serial buffer into the local buffer
  int buffer_len = 0;
  while (Serial.available() > 0) {
    char ch = Serial.read();
    if (ch == '\n') break;
    buffer[buffer_len++] = ch;
  }
  buffer[buffer_len] = 0;
  return buffer_len;
}


// The format of piece is <pieceNum>,<rotation>,<x>,<y>
void displayPiece(const char *piece) {
  int pieceNum = -1;
  int rotation = 0;
  if (strlen(piece) >= 1) {
    pieceNum = piece[0] - '0';
  }
  if (strlen(piece) >= 3) {
    rotation = piece[2] - '0';
  }
  
  int colour = -1;
  switch (pieceNum) {
    case 0: colour = cyan; break;
    case 1: colour = blue; break;
    case 2: colour = white; break;
    case 3: colour = yellow; break;
    case 4: colour = green; break;
    case 5: colour = magenta; break;
    case 6: colour = red; break;
  }
  
  if (colour >= 0) {
    digiPixel.clearScreen();
    for (int x = 0; x < 4; x++) {
      for (int y = 0; y < 4; y++) {
        if (PIECES[rotation][y][pieceNum][x] != ' ') {
          #ifdef DOUBLE_PIXEL
          digiPixel.setPixel((2*x),   7-(2*y),   colour);
          digiPixel.setPixel((2*x)+1, 7-(2*y)-1, colour);
          digiPixel.setPixel((2*x),   7-(2*y)-1, colour);
          digiPixel.setPixel((2*x)+1, 7-(2*y),   colour);
          #else
          digiPixel.setPixel(x+2, 5-y, colour);
          #endif
        }
      }
    }
  }
  
}


void loop(void) {
  // Check for button presses
  unsigned long now = millis();
  if (now - last_loop >= 50) {
    last_loop = now;
    digiPixel.saveButtonStates();
    checkButtons();
  }
  
  // Check for serial comms
  if (Serial.available() > 0) {
    char buffer[80] = "";
    if (checkSerial(buffer) > 0) {
      displayPiece(buffer);
    }
  }

  // Update the screen
  digiPixel.drawScreen();
}

