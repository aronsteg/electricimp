class NeoPixels {
    
    // when instantiated, the neopixel class will fill this array with blobs to 
    // represent the waveforms to send the numbers 0 to 255. This allows the blobs to be
    // copied in directly, instead of being built for each pixel - which makes the class faster.
    bits            = null;
    
    // Like bits, this blob holds the waveform to send the color [0,0,0], to clear pixels faster
    clearblob       = null; 
    
    // private variables passed into the constructor
    spi             = null; // imp SPI interface (pre-configured)
    width           = null; // the maximum X dimension
    height          = null; // the maximum Y dimension
    brightness      = 1.0;  // the brightness scale, 0.0 = off, 1.0 = 100% brightness
    frameSize       = null; // number of pixels per frame (height x width)
    frame           = null; // a blob to hold the current frame buffer
    canvas          = null; // 2d array holding the next buffer to be drawn
    snapshot        = null; // holds a copy of the canvas for quick drawing of a background/template
    cache           = null; // holds the hsl2rgb cache
    
    // _spi - A configured spi (MSB_FIRST, 7.5MHz)
    // _width - X pixels wide
    // _height - Y pixels high
    constructor(_spi, _width, _height = 1) {
        // This class uses SPI to emulate the newpixels' one-wire protocol. 
        // This requires one byte per bit to send data at 7.5 MHz via SPI. 
        // These consts define the "waveform" to represent a zero or one 
        const SPICLK          = 7500; // kHz
        const ZERO            = 0xC0;
        const ONE             = 0xF8;
        const BYTESPERPIXEL   = 24;
        
        spi = _spi;
        width = _width;
        height= _height;
        
        spi.configure(MSB_FIRST, SPICLK);

        frameSize = width * height;
        frame = blob(frameSize*BYTESPERPIXEL + 1);

        clearblob = blob(BYTESPERPIXEL);
        cache = {};
        
        // prepare the bits array and the clearblob blob
        initialize();
        
        // Blank the screen
        clear();
        write();
        
    }
    
    // fill the array of representative 1-wire waveforms. 
    // done by the constructor at instantiation.
    function initialize() {
        // fill the bits array first
        bits = array(256);
        for (local i = 0; i < 256; i++) {
            local valblob = blob(BYTESPERPIXEL / 3);
            valblob.writen((i & 0x80) ? ONE:ZERO,'b');
            valblob.writen((i & 0x40) ? ONE:ZERO,'b');
            valblob.writen((i & 0x20) ? ONE:ZERO,'b');
            valblob.writen((i & 0x10) ? ONE:ZERO,'b');
            valblob.writen((i & 0x08) ? ONE:ZERO,'b');
            valblob.writen((i & 0x04) ? ONE:ZERO,'b');
            valblob.writen((i & 0x02) ? ONE:ZERO,'b');
            valblob.writen((i & 0x01) ? ONE:ZERO,'b');
            bits[i] = valblob;
        }
        
        // now fill the clearblob
        for(local j = 0; j < BYTESPERPIXEL; j++) {
            clearblob.writen(ZERO, 'b');
        }
        
        // finally, prepare the canvas
        canvas = array(width);
        for (local x = 0; x < width; x++) {
            canvas[x] = array(height);
            for (local y = 0; y < height; y++) {
                canvas[x][y] = null;
            }
        }
    }

    // draw a single pixel onto the canvas
    function drawPixel(x, y, color) { // or pdp
        if (x >= 0 && x < width && y >= 0 && y < height) {
            canvas[x][y] = color;
        }
    }
    
    // draw a box or line on the canvas
    function drawBox(x1, y1, x2, y2, color) { // or pdb
        // Swap the coordinates to its always drawing uphill
        if (x2 < x1) { local x3 = x1; x1 = x2; x2 = x3 }
        if (y2 < y1) { local y3 = y1; y1 = y2; y2 = y3 }

        for (local x = x1; x <= x2; x++) {
            for (local y = y1; y <= y2; y++) {
                if (x >= 0 && x < width && y >= 0 && y < height) {
                    canvas[x][y] = color;
                }
            }
        }
    }
    
    // wipes the canvas to a single colour (or black)
    function clear(color = null) { // or pcf
        for (local x = 0; x < width; x++) {
            for (local y = 0; y < height; y++) {
                canvas[x][y] = color;
            }
        }
    }
    
    // sends the canvas to the neopixels
    function write() {
        
        local color, x, y, yy, alt = true;
        local fwb = frame.writeblob.bindenv(frame);

        frame.seek(0);
        for (x = 0; x < width; x++) {
            alt = !alt;
            for (y = 0; y < height; y++) {
                // Alternate direction of every alternate row
                yy = alt ? (height - y - 1) : y;
                color = canvas[x][yy];
                if (color) {
                    fwb(bits[(color[1] * brightness).tointeger()]);
                    fwb(bits[(color[0] * brightness).tointeger()]);
                    fwb(bits[(color[2] * brightness).tointeger()]);    
                } else {
                    fwb(clearblob);
                }
            }
        }
        frame.writen(0, 'b'); // Drive MOSI low
        
        // All done. Send.
        spi.write(frame);
    }
    
    // stores the current canvas for future fast use
    function storeSnapshot() {
        snapshot = array(width);
        for (local x = 0; x < width; x++) {
            snapshot[x] = clone canvas[x];
        }
    }
    
    // restores a snapshot as the primary canvas
    function restoreSnapshot() {
        if (snapshot) {
            for (local x = 0; x < width; x++) {
                canvas[x] = clone snapshot[x];
            }
            return true;
        } else {
            return false;
        }
    }
    
    // scales brightness down by a percentage
    function scaleBrightness(scale) {
        brightness = scale;
        write();
    }
    
        
    /**
     * Converts an HSL color value to RGB. Conversion formula
     * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
     * Assumes h, s, and l are contained in the set [0, 255] and
     * returns r, g, and b in the set [0, 255].
     *
     * @param   Number  h       The hue
     * @param   Number  s       The saturation
     * @param   Number  l       The lightness
     * @return  Array           The RGB representation
     */
    function hsl2rgb(h, s, l, _cache=false) {
        
        local cachekey = format("%02X%02X%02X", h, s, l);
        if (cachekey in cache) return cache[cachekey];
        
        local hue2rgb = function(p, q, t) {
            if (t < 0.0) t += 1.0;
            if (t > 1.0) t -= 1.0;
            if (t < 0.16666666) return p + (q - p) * 6.0 * t;
            if (t < 0.5) return q;
            if (t < 0.66666666) return p + (q - p) * (0.66666666 - t) * 6.0;
            return p;
        }
        
        
        local r = 0.0;
        local g = 0.0;
        local b = 0.0;
        
        h /= 255.0;
        s /= 255.0;
        l /= 255.0;
        
        if (s == 0) {
            r = g = b = l; // achromatic
        } else if (l == 0) {
            r = g = b = l; // off
        } else {
            local q = l < 0.5 ? l * (1.0 + s) : l + s - l * s;
            local p = 2.0 * l - q;
            r = hue2rgb(p, q, h + 0.33333333);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 0.33333333);
        }
    
        local rgb = [math.floor(r*255.0), math.floor(g*255.0), math.floor(b*255.0)];
        if (_cache) cache[cachekey] <- rgb;
        
        return rgb;
    }
}


neo <- NeoPixels(hardware.spi257, 16, 16);
neo.scaleBrightness(0.2);


Colors <- {
    "X":   [255, 255, 255], 
    "b":   [0,   255, 255], 
    "B":   [0,     0, 255], 
    "O":   [255, 120,   0], 
    "Y":   [255, 255,   0], 
    "G":   [0,   255,   0], 
    "P":   [147, 112, 216], 
    "R":   [255,   0,   0],
    "IN":  [8,     0,   8],
    "OUT": [15,   15,  15]
};

pieces <- [];
for (local i = 0; i < 4; i++) {
    pieces.push([]);
    for (local j = 0; j < 4; j++) { 
        pieces[i].push("");
    }
}

pieces[0][0] = [ "    ",   "    ",   "    ",   "    ",   "    ",   "    ",   "    " ];
pieces[0][1] = [ "    ",   "B   ",   "  O ",   " YY ",   " GG ",   " P  ",   "RR  " ];
pieces[0][2] = [ "bbbb",   "BBB ",   "OOO ",   " YY ",   "GG  ",   "PPP ",   " RR " ];
pieces[0][3] = [ "    ",   "    ",   "    ",   "    ",   "    ",   "    ",   "    " ];

pieces[1][0] = [ " b  ",   "    ",   "    ",   "    ",   "    ",   "    ",   "  R " ];
pieces[1][1] = [ " b  ",   " B  ",   "OO  ",   " YY ",   " G  ",   " P  ",   " RR " ];
pieces[1][2] = [ " b  ",   " B  ",   " O  ",   " YY ",   " GG ",   " PP ",   " R  " ];
pieces[1][3] = [ " b  ",   "BB  ",   " O  ",   "    ",   "  G ",   " P  ",   "    " ];

pieces[2][0] = [ "    ",   "    ",   "    ",   "    ",   "    ",   "    ",   "    " ];
pieces[2][1] = [ "    ",   "    ",   "    ",   " YY ",   " GG ",   "    ",   "RR  " ];
pieces[2][2] = [ "bbbb",   "BBB ",   "OOO ",   " YY ",   "GG  ",   "PPP ",   " RR " ];
pieces[2][3] = [ "    ",   "  B ",   "O   ",   "    ",   "    ",   " P  ",   "    " ];

pieces[3][0] = [ " b  ",   "    ",   "    ",   "    ",   "    ",   "    ",   "  R " ];
pieces[3][1] = [ " b  ",   " BB ",   " O  ",   " YY ",   " G  ",   " P  ",   " RR " ];
pieces[3][2] = [ " b  ",   " B  ",   " O  ",   " YY ",   " GG ",   "PP  ",   " R  " ];
pieces[3][3] = [ " b  ",   " B  ",   " OO ",   "    ",   "  G ",   " P  ",   "    " ];



const YOFFSET = 0;
const XOFFSET = 3;

function draw_board(row, pixels = "") {
    row = row.tointeger() + YOFFSET;
    if (pixels == null) {
        neo.drawBox(row, 0,       row, 15,         Colors["OUT"])
        neo.drawBox(row, XOFFSET, row, 15 - XOFFSET, Colors["IN"]);
    } else {
        neo.drawBox(row, 0,  row, 2,  Colors["OUT"])
        neo.drawBox(row, 3,  row, 12, Colors["IN"])
        neo.drawBox(row, 13, row, 15, Colors["OUT"])
        
        for (local col = 0; col < pixels.len(); col++) {
            local pixel = pixels[col];

            if (pixel == ' ') {
                neo.drawPixel(row, col + XOFFSET, Colors["IN"])
            } else {
                neo.drawPixel(row, col + XOFFSET, Colors[pixel.tochar()])
            }
        }
    }
}


function draw_piece(piece, erase = false) {
    local pieceData = pieces[piece.rotation];
    local y = piece.y;
    
    foreach(row in pieceData) {
        local x = piece.x;
        foreach (pixel in row[piece.pieceNum]) {
            if (pixel != ' ') {
                neo.drawPixel(y + YOFFSET, x + XOFFSET, erase ? Colors["IN"] : Colors[pixel.tochar()]);
            }
            x++;
        }
        y++;
    }
    
}

function clear_board() {
    for (local row = 0; row < 16; row++) {
        draw_board(row);
    }
}

last_piece <- null;

agent.on("restart", function (piece) {
    clear_board();
});

agent.on("board", function (board) {
    if (board.path == "/player0/board") {
        if (board.data == null) {
            // Wipe the whole board clean
            clear_board();
        } else {
            // Draw all the rows provided
            foreach (row, pixels in board.data) {
                draw_board(row, pixels);
            }
        }
    } else {
        local row = split(board.path, "/").pop();
        draw_board(row, board.data);
    }
    
    // Done with the changes
    neo.write();
});

agent.on("piece", function (piece) {
    if (piece.path == "/player0/piece") {
        if (piece.data == null) {
            if (last_piece != null) {
                draw_piece(last_piece, true)
                last_piece = null;
            }
            return;
        }
        if (last_piece != null) {
            if (last_piece.y > piece.data.y) {
                // We have a new piece at the top, let the previous one live
                last_piece = null;
            } else {
                // Remove the old piece
                draw_piece(last_piece, true)
            }
        }
        
        // Draw the new piece 
        draw_piece(piece.data);
        
        // Save the piece location for the next frame
        last_piece = clone piece.data;

        // Done with the changes
        neo.write();
    }
});


// Draw the initial state of the board
clear_board();

// Notify the agent I am back and ready for updates
agent.send("online", true);