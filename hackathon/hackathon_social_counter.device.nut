// Hackathong social counter - Device code

const VERSION = 0.01;        
const ANIMATE_TIME = 0.04;   // 33 frames (or pixels) per seconds

const VSBL_COLUMNS = 64;     // 64 columns = 8 virtual 8-pixel wide panels
const REAL_COLUMNS = 120;    // 120 columns = 15 virtual 8-pixel wide panels
const VIRT_COLUMNS = 800;    // 800 columns = 100 virtual 8-pixel wide panels

const CMD_UPDATE = 0x80;
const CMD_SCROLL_LEFT = 0x81;
const CMD_SCROLL_RIGHT = 0x82;
const CMD_SCROLL_UP = 0x83;
const CMD_SCROLL_DOWN = 0x84;
const CMD_BRUSH = 0x85;
const CMD_SELECT_1 = 0x90;
const CMD_SET_LCD = 0xA0;
const CMD_SET_LED = 0xA1;

const DEV_PANEL_1 = 0x00;
const DEV_PANEL_2 = 0x01;
const DEV_SPEAKER = 0x00;
const DEV_MICROPHONE = 0x01;
const DEV_RF433 = 0x02;
const DEV_LED = 0x01;
const DEV_LED_MIN = 0x00;
const DEV_LED_MAX = 0x05;
const DEV_LCD_1 = 0x00;
const DEV_LCD_2 = 0x01;
const DEV_LCD_3 = 0x02;
const DEV_LCD_4 = 0x03;


// =============================================================================
class the_hardware {
   
    uart = null;
    aniTimer = null;
    pixel_rows = null;
    pixel_columns = null;
    pixel_columns_used = 0;
    column_offset = 0;
    wrap_callback = null;
    play_all_files_callback = null;
    scroll = false;
    play_pos = 0;
    powersave = false;
    locked = false;
    
    // -------------------------------------------------------------------------
    constructor() {
        // Animation
        aniTimer = imp.wakeup(ANIMATE_TIME, animate.bindenv(this))
        
        // Prepare the pixel buffer
        pixel_columns = blob(VIRT_COLUMNS);
        pixel_rows = blob(REAL_COLUMNS);
        
        // Pins 6&E - UART to MCU
        uart = hardware.uart6E;
        uart.configure(57600, 8, PARITY_NONE, 1, NO_CTSRTS);
        
        // Disable power save mode
        imp.setpowersave(false);        
    
        // Enable blinkup for longer
        if ("enableblinkup" in imp) imp.enableblinkup(true);
    }


    // -------------------------------------------------------------------------
    function send(command, panel, data, start = 0x00) {
        
        local length = 1;
        switch (typeof data)
        {
            case "integer":
                data = data.tochar();
                break;
                
            case "blob":
                length = data.len();
                break;
                
            case "array":
            case "string":
                length = data.len()-1;
                break;
            
            default:
                return;
        }
        local checksum = 0x00;
        
        
        uart.configure(57600, 8, PARITY_NONE, 1, NO_CTSRTS); // A temporary hack
        uart.write(command);
        uart.write(panel);
        uart.write(start);
        uart.write(length);
        uart.write(data);
        uart.write(checksum);
    }
    
    
    // -------------------------------------------------------------------------
    function clear(brush = 0x00) {
        send(CMD_BRUSH, DEV_PANEL_1, brush);
    }
    
    // -------------------------------------------------------------------------
    function update(bitmap) {
        send(CMD_UPDATE, DEV_PANEL_1, bitmap);
    }
    
    // -------------------------------------------------------------------------
    function left(column = 0x00) {
        send(CMD_SCROLL_LEFT, DEV_PANEL_1, column);
    }
    
    // -------------------------------------------------------------------------
    function right(column = 0x00) {
        send(CMD_SCROLL_RIGHT, DEV_PANEL_1, column);
    }
    
    // -------------------------------------------------------------------------
    function up(row = null) {
        if (row == null) row = "\x00\x00\x00\x00\x00\x00\x00\x00" + "\x00\x00\x00\x00\x00\x00\x00";
        send(CMD_SCROLL_UP, DEV_PANEL_1, row);
    }
    
    // -------------------------------------------------------------------------
    function down(row = null) {
        if (row == null) row = "\x00\x00\x00\x00\x00\x00\x00\x00" + "\x00\x00\x00\x00\x00\x00\x00";
        send(CMD_SCROLL_DOWN, DEV_PANEL_1, row);
    }
        
    // -------------------------------------------------------------------------
    function set_text(message, do_scroll = false, callback = null) {
    
        // This should speed things
        local pcw = pixel_columns.writen.bindenv(pixel_columns);
        
        // Look up the columns for each character and write it to the column buffer
        pixel_columns_used = 0;
        pixel_columns.seek(0, 'b')
        foreach (ch in message) {
            
            // Add the character
            local bitmap = char_to_bin(ch);
            foreach (col in bitmap) {
                pcw(col, 'b');
                
                if (++pixel_columns_used >= pixel_columns.len()-1) {
                    log("We have maxed out the pixel_column buffer. Now thats a big message!")
                    break;
                }
            }
            
            if (pixel_columns_used >= pixel_columns.len()-1) break;
                
            // Add a space
            pcw(0x00, 'b');
            pixel_columns_used++;
        }
        
        // Auto scroll
        if (pixel_columns_used > VSBL_COLUMNS) do_scroll = true;
        
        // Wipe out the rest of the columns
        for (local i = pixel_columns.tell(); i < pixel_columns.len(); i++) {
            pcw(0x00, 'b');
        }
        // log("Pixel count is " + (pixel_columns.len() <= VIRT_COLUMNS) ? "ok" : "TOO BIG");

        // Save the parameters in the object
        wrap_callback = callback;
        scroll = do_scroll;
        
        // Work out where to start drawing
        if (scroll) {
            // Set the column offset to the far right so it can be animated in
            column_offset = -VSBL_COLUMNS;
        } else {
            // Draw the first frame by translating columns to rows
            update(columns_to_rows(pixel_columns));
            
            // Execute the callback
            if (wrap_callback) {
                // We want the callback to be cleared BEFORE we execute it
                local callback = wrap_callback;
                wrap_callback = null;
                callback();
            }

        }
        
    }

    
    // -------------------------------------------------------------------------
    function animate() {
        aniTimer = imp.wakeup(ANIMATE_TIME, animate.bindenv(this))        
        
        // Shortcut for blanking the screen
        if (pixel_columns_used == 0) {
            clear();
            return;
        }
        
        // Don't animate if scroll is not set
        if (!scroll) {
            return;
        }
        
        if (column_offset == -VSBL_COLUMNS) {
            // Redraw the full first frame by translating columns to rows
            update(columns_to_rows(pixel_columns, column_offset));
        } else {
            // Shift a new column into the display
            // server.log("Pixel column: " + (REAL_COLUMNS + column_offset - 1))
            left(reverse(pixel_columns[REAL_COLUMNS + column_offset - 1]))
        }
            
        // Shift left, wrapping at the end
        column_offset++;
        if (pixel_columns_used > 0 && (column_offset == pixel_columns_used || REAL_COLUMNS + column_offset - 1 == VIRT_COLUMNS)) {
            // Execute the callback if there is one
            if (wrap_callback) {
                // We want the callback to be cleared BEFORE we execute it
                local callback = wrap_callback;
                wrap_callback = null;
                callback();
            }

            column_offset = -VSBL_COLUMNS;
        }
        
    }

    
    // -------------------------------------------------------------------------
    function columns_to_rows(pixel_columns, column_offset = 0) {
        
        local prw = pixel_rows.writen.bindenv(pixel_rows);
        local prd = 0x0;
        pixel_rows.seek(0, 'b');
        for (local r = 0; r < 8; r++) {         // For each row
            for (local c = 0; c < REAL_COLUMNS && c < VIRT_COLUMNS; c++) {
                // Mask out the row and column intersection from the column bitmap
                local pcd = 0x0;
                if (c + column_offset < 0) {
                    pcd = 0x0;
                } else if (c + column_offset >= pixel_columns.len()) {
                    pcd = 0x0;
                } else {
                    pcd = pixel_columns[c + column_offset];
                }
                local row_mask = 0x01 << (7-r);
                local col_shift = (c % 8 - r);
                local pixel = 0;
                if (col_shift < 0) {
                    pixel = (pcd & row_mask) << -col_shift;
                } else {
                    pixel = (pcd & row_mask) >> col_shift;                            
                }
                
                // Add them up to make a row bitmap
                prd = prd | pixel;
                
                // Every panel boundary, start again
                if (c % 8 == 7) {
                    prw(prd, 'b');
                    prd = 0x0;
                }
            }
        }
        
        return pixel_rows;
    }
    

    
}




// =============================================================================

// -----------------------------------------------------------------------------
// Logs a message prefixed with "Device: "
function log(message)
{
    server.log("Device: " + message);
}
    
// -----------------------------------------------------------------------------
// Flips a bitmap top-to-bottom / left-to-right.
const reverse_map = "\x00\x80\x40\xc0\x20\xa0\x60\xe0\x10\x90\x50\xd0\x30\xb0\x70\xf0\x08\x88\x48\xc8\x28\xa8\x68\xe8\x18\x98\x58\xd8\x38\xb8\x78\xf8\x04\x84\x44\xc4\x24\xa4\x64\xe4\x14\x94\x54\xd4\x34\xb4\x74\xf4\x0c\x8c\x4c\xcc\x2c\xac\x6c\xec\x1c\x9c\x5c\xdc\x3c\xbc\x7c\xfc\x02\x82\x42\xc2\x22\xa2\x62\xe2\x12\x92\x52\xd2\x32\xb2\x72\xf2\x0a\x8a\x4a\xca\x2a\xaa\x6a\xea\x1a\x9a\x5a\xda\x3a\xba\x7a\xfa\x06\x86\x46\xc6\x26\xa6\x66\xe6\x16\x96\x56\xd6\x36\xb6\x76\xf6\x0e\x8e\x4e\xce\x2e\xae\x6e\xee\x1e\x9e\x5e\xde\x3e\xbe\x7e\xfe\x01\x81\x41\xc1\x21\xa1\x61\xe1\x11\x91\x51\xd1\x31\xb1\x71\xf1\x09\x89\x49\xc9\x29\xa9\x69\xe9\x19\x99\x59\xd9\x39\xb9\x79\xf9\x05\x85\x45\xc5\x25\xa5\x65\xe5\x15\x95\x55\xd5\x35\xb5\x75\xf5\x0d\x8d\x4d\xcd\x2d\xad\x6d\xed\x1d\x9d\x5d\xdd\x3d\xbd\x7d\xfd\x03\x83\x43\xc3\x23\xa3\x63\xe3\x13\x93\x53\xd3\x33\xb3\x73\xf3\x0b\x8b\x4b\xcb\x2b\xab\x6b\xeb\x1b\x9b\x5b\xdb\x3b\xbb\x7b\xfb\x07\x87\x47\xc7\x27\xa7\x67\xe7\x17\x97\x57\xd7\x37\xb7\x77\xf7\x0f\x8f\x4f\xcf\x2f\xaf\x6f\xef\x1f\x9f\x5f\xdf\x3f\xbf\x7f\xff";
function reverse(x)
{
    return reverse_map[x];
}

// -----------------------------------------------------------------------------
function char_to_bin(c) {
    switch (c) {
      case ' ':  return "\x00";                      // SP ----- -O--- OO-OO ----- -O--- OO--O -O--- -O---
      case '!':  return "\xfa";                      // !  ----- -O--- OO-OO -O-O- -OOO- OO--O O-O-- -O---
      case '"':  return "\xe0\xc0\x00\xe0\xc0";      // "  ----- -O--- O--O- OOOOO O---- ---O- O-O-- -----
      case '#':  return "\x24\x7e\x24\x7e\x24";      // #  ----- -O--- ----- -O-O- -OO-- --O-- -O--- -----
      case '$':  return "\x24\xd4\x56\x48";          // $  ----- -O--- ----- -O-O- ---O- -O--- O-O-O -----
      case '%':  return "\xc6\xc8\x10\x26\xc6";      // %  ----- ----- ----- OOOOO OOO-- O--OO O--O- -----
      case '&':  return "\x6c\x92\x6a\x04\x0a";      // &  ----- -O--- ----- -O-O- --O-- O--OO -OO-O -----
      case '\'': return "\xc0";                      // '  ----- ----- ----- ----- ----- ----- ----- -----
    //
      case '(':  return "\x7c\x82";                  // (  ---O- -O--- ----- ----- ----- ----- ----- -----
      case ')':  return "\x82\x7c";                  // )  --O-- --O-- -O-O- --O-- ----- ----- ----- ----O
      case '*':  return "\x10\x7c\x38\x7c\x10";      // *  --O-- --O-- -OOO- --O-- ----- ----- ----- ---O-
      case '+':  return "\x10\x10\x7c\x10\x10";      // +  --O-- --O-- OOOOO OOOOO ----- OOOOO ----- --O--
      case ',':  return "\x06\x07";                  // ,  --O-- --O-- -OOO- --O-- ----- ----- ----- -O---
      case '-':  return "\x10\x10\x10\x10\x10";      // -  --O-- --O-- -O-O- --O-- -OO-- ----- -OO-- O----
      case '.':  return "\x06\x06";                  // .  ---O- -O--- ----- ----- -OO-- ----- -OO-- -----
      case '/':  return "\x04\x08\x10\x20\x40";      // /  ----- ----- ----- ----- --O-- ----- ----- -----
    //
      case '0':  return "\x7c\x8a\x92\xa2\x7c";      // 0  -OOO- --O-- -OOO- -OOO- ---O- OOOOO --OOO OOOOO
      case '1':  return "\x00\x42\xfe\x02\x00";      // 1  O---O -OO-- O---O O---O --OO- O---- -O--- ----O
      case '2':  return "\x46\x8a\x92\x92\x62";      // 2  O--OO --O-- ----O ----O -O-O- O---- O---- ---O-
      case '3':  return "\x44\x92\x92\x92\x6c";      // 3  O-O-O --O-- --OO- -OOO- O--O- OOOO- OOOO- --O--
      case '4':  return "\x18\x28\x48\xfe\x08";      // 4  OO--O --O-- -O--- ----O OOOOO ----O O---O -O---
      case '5':  return "\xf4\x92\x92\x92\x8c";      // 5  O---O --O-- O---- O---O ---O- O---O O---O -O---
      case '6':  return "\x3c\x52\x92\x92\x8c";      // 6  -OOO- -OOO- OOOOO -OOO- ---O- -OOO- -OOO- -O---
      case '7':  return "\x80\x8e\x90\xa0\xc0";      // 7  ----- ----- ----- ----- ----- ----- ----- -----
    //
      case '8':  return "\x6c\x92\x92\x92\x6c";      // 8  -OOO- -OOO- ----- ----- ---O- ----- -O--- -OOO-
      case '9':  return "\x60\x92\x92\x94\x78";      // 9  O---O O---O ----- ----- --O-- ----- --O-- O---O
      case ':':  return "\x36\x36";                  // :  O---O O---O -OO-- -OO-- -O--- OOOOO ---O- O---O
      case ';':  return "\x36\x37";                  // ;  -OOO- -OOOO -OO-- -OO-- O---- ----- ----O --OO-
      case '<':  return "\x10\x28\x44\x82";          // <  O---O ----O ----- ----- -O--- ----- ---O- --O--
      case '=':  return "\x24\x24\x24\x24\x24";      // =  O---O ---O- -OO-- -OO-- --O-- OOOOO --O-- -----
      case '>':  return "\x82\x44\x28\x10";          // >  -OOO- -OO-- -OO-- -OO-- ---O- ----- -O--- --O--
      case '?':  return "\x60\x80\x9a\x90\x60";      // ?  ----- ----- ----- --O-- ----- ----- ----- -----
    //
      case '@':  return "\x7c\x82\xba\xaa\x78";      // @  -OOO- -OOO- OOOO- -OOO- OOOO- OOOOO OOOOO -OOO-
      case 'A':  return "\x7e\x90\x90\x90\x7e";      // A  O---O O---O O---O O---O O---O O---- O---- O---O
      case 'B':  return "\xfe\x92\x92\x92\x6c";      // B  O-OOO O---O O---O O---- O---O O---- O---- O----
      case 'C':  return "\x7c\x82\x82\x82\x44";      // C  O-O-O OOOOO OOOO- O---- O---O OOOO- OOOO- O-OOO
      case 'D':  return "\xfe\x82\x82\x82\x7c";      // D  O-OOO O---O O---O O---- O---O O---- O---- O---O
      case 'E':  return "\xfe\x92\x92\x92\x82";      // E  O---- O---O O---O O---O O---O O---- O---- O---O
      case 'F':  return "\xfe\x90\x90\x90\x80";      // F  -OOO- O---O OOOO- -OOO- OOOO  OOOOO O---- -OOO-
      case 'G':  return "\x7c\x82\x92\x92\x5c";      // G  ----- ----- ----- ----- ----- ----- ----- -----
    //
      case 'H':  return "\xfe\x10\x10\x10\xfe";      // H  O---O -OOO- ----O O---O O---- O---O O---O -OOO-
      case 'I':  return "\x82\xfe\x82";              // I  O---O --O-- ----O O--O- O---- OO-OO OO--O O---O
      case 'J':  return "\x0c\x02\x02\x02\xfc";      // J  O---O --O-- ----O O-O-- O---- O-O-O O-O-O O---O
      case 'K':  return "\xfe\x10\x28\x44\x82";      // K  OOOOO --O-- ----O OO--- O---- O---O O--OO O---O
      case 'L':  return "\xfe\x02\x02\x02\x02";      // L  O---O --O-- O---O O-O-- O---- O---O O---O O---O
      case 'M':  return "\xfe\x40\x20\x40\xfe";      // M  O---O --O-- O---O O--O- O---- O---O O---O O---O
      case 'N':  return "\xfe\x40\x20\x10\xfe";      // N  O---O -OOO- -OOO- O---O OOOOO O---O O---O -OOO-
      case 'O':  return "\x7c\x82\x82\x82\x7c";      // O  ----- ----- ----- ----- ----- ----- ----- -----
    //
      case 'P':  return "\xfe\x90\x90\x90\x60";      // P  OOOO- -OOO- OOOO- -OOO- OOOOO O---O O---O O---O
      case 'Q':  return "\x7c\x82\x92\x8c\x7a";      // Q  O---O O---O O---O O---O --O-- O---O O---O O---O
      case 'R':  return "\xfe\x90\x90\x98\x66";      // R  O---O O---O O---O O---- --O-- O---O O---O O-O-O
      case 'S':  return "\x64\x92\x92\x92\x4c";      // S  OOOO- O-O-O OOOO- -OOO- --O-- O---O O---O O-O-O
      case 'T':  return "\x80\x80\xfe\x80\x80";      // T  O---- O--OO O--O- ----O --O-- O---O O---O O-O-O
      case 'U':  return "\xfc\x02\x02\x02\xfc";      // U  O---- O--O- O---O O---O --O-- O---O -O-O- O-O-O
      case 'V':  return "\xf8\x04\x02\x04\xf8";      // V  O---- -OO-O O---O -OOO- --O-- -OOO- --O-- -O-O-
      case 'W':  return "\xfc\x02\x3c\x02\xfc";      // W  ----- ----- ----- ----- ----- ----- ----- -----
    //
      case 'X':  return "\xc6\x28\x10\x28\xc6";      // O  O---O O---O OOOOO -OOO- ----- -OOO- --O-- -----
      case 'Y':  return "\xe0\x10\x0e\x10\xe0";      // Y  O---O O---O ----O -O--- O---- ---O- -O-O- -----
      case 'Z':  return "\x86\x8a\x92\xa2\xc2";      // Z  -O-O- O---O ---O- -O--- -O--- ---O- O---O -----
      case '[':  return "\xfe\x82\x82";              // [  --O-- -O-O- --O-- -O--- --O-- ---O- ----- -----
      case '\\': return "\x40\x20\x10\x08\x04";      // \  -O-O- --O-- -O--- -O--- ---O- ---O- ----- -----
      case ']':  return "\x82\x82\xfe";              // ]  O---O --O-- O---- -O--- ----O ---O- ----- -----
      case '^':  return "\x20\x40\x80\x40\x20";      // ^  O---O --O-- OOOOO -OOO- ----- -OOO- ----- OOOOO
      case '_':  return "\x02\x02\x02\x02\x02";      // _  ----- ----- ----- ----- ----- ----- ----- -----
    //
      case '`':  return "\xc0\xe0";                  // `  -OO-- ----- O---- ----- ----O ----- --OOO -----
      case 'a':  return "\x04\x2a\x2a\x2a\x1e";      // a  -OO-- ----- O---- ----- ----O ----- -O--- -----
      case 'b':  return "\xfe\x22\x22\x22\x1c";      // b  --O-- -OOO- OOOO- -OOO- -OOOO -OOO- -O--- -OOOO
      case 'c':  return "\x1c\x22\x22\x22";          // c  ----- ----O O---O O---- O---O O---O OOOO- O---O
      case 'd':  return "\x1c\x22\x22\x22\xfc";      // d  ----- -OOOO O---O O---- O---O OOOO- -O--- O---O
      case 'e':  return "\x1c\x2a\x2a\x2a\x10";      // e  ----- O---O O---O O---- O---O O---- -O--- -OOOO
      case 'f':  return "\x10\x7e\x90\x90\x80";      // f  ----- -OOOO OOOO- -OOO- -OOOO -OOO- -O--- ----O
      case 'g':  return "\x18\x25\x25\x25\x3e";      // g  ----- ----- ----- ----- ----- ----- ----- -OOO-
    //
      case 'h':  return "\xfe\x20\x20\x20\x1e";      // h  O---- -O--- ----O O---- O---- ----- ----- -----
      case 'i':  return "\xbe\x02";                  // i  O---- ----- ----- O---- O---- ----- ----- -----
      case 'j':  return "\x02\x01\x01\x21\xbe";      // j  OOOO- -O--- ---OO O--O- O---- OO-O- OOOO- -OOO-
      case 'k':  return "\xfe\x08\x14\x22";          // k  O---O -O--- ----O O-O-- O---- O-O-O O---O O---O
      case 'l':  return "\xfe\x02";                  // l  O---O -O--- ----O OO--- O---- O-O-O O---O O---O
      case 'm':  return "\x3e\x20\x18\x20\x1e";      // m  O---O -O--- ----O O-O-- O---- O---O O---O O---O
      case 'n':  return "\x3e\x20\x20\x20\x1e";      // n  O---O -OO-- O---O O--O- OO--- O---O O---O -OOO-
      case 'o':  return "\x1c\x22\x22\x22\x1c";      // o  ----- ----- -OOO- ----- ----- ----- ----- -----
    //
      case 'p':  return "\x3f\x22\x22\x22\x1c";      // p  ----- ----- ----- ----- ----- ----- ----- -----
      case 'q':  return "\x1c\x22\x22\x22\x3f";      // q  ----- ----- ----- ----- -O--- ----- ----- -----
      case 'r':  return "\x22\x1e\x22\x20\x10";      // r  OOOO- -OOOO O-OO- -OOO- OOOO- O--O- O---O O---O
      case 's':  return "\x12\x2a\x2a\x2a\x04";      // s  O---O O---O -O--O O---- -O--- O--O- O---O O---O
      case 't':  return "\x20\x7c\x22\x22\x04";      // t  O---O O---O -O--- -OOO- -O--- O--O- O---O O-O-O
      case 'u':  return "\x3c\x02\x02\x3e";          // u  O---O O---O -O--- ----O -O--O O--O- -O-O- OOOOO
      case 'v':  return "\x38\x04\x02\x04\x38";      // v  OOOO- -OOOO OOO-- OOOO- --OO- -OOO- --O-- -O-O-
      case 'w':  return "\x3c\x06\x0c\x06\x3c";      // w  O---- ----O ----- ----- ----- ----- ----- -----
    //
      case 'x':  return "\x22\x14\x08\x14\x22";      // x  ----- ----- ----- ---OO --O-- OO--- -O-O- -OO--
      case 'y':  return "\x39\x05\x06\x3c";          // y  ----- ----- ----- --O-- --O-- --O-- O-O-- O--O-
      case 'z':  return "\x26\x2a\x2a\x32";          // z  O---O O--O- OOOO- --O-- --O-- --O-- ----- O--O-
      case '{':  return "\x10\x7c\x82\x82";          // {  -O-O- O--O- ---O- -OO-- ----- --OO- ----- -OO--
      case '|':  return "\xee";                      // |  --O-- O--O- -OO-- --O-- --O-- --O-- ----- -----
      case '}':  return "\x82\x82\x7c\x10";          // }  -O-O- -OOO- O---- --O-- --O-- --O-- ----- -----
      case '~':  return "\x40\x80\x40\x80";          // ~  O---O --O-- OOOO- ---OO --O-- OO--- ----- -----
      case '_':  return "\x60\x90\x90\x60";          // _  ----- OO--- ----- ----- ----- ----- ----- -----
    //
      case '\t': return "\x00\x00\x00\x00\x00\x00\x00\x00"; // Tab
      case '\n': return "\x04\x08\x1c\xA2\x55\x45\x55\xA2\x1c\x08\x30\x30";             // Duck
      default:   return "\x00\x00\x00\x00\x00";             // Blank    
    }

}





// =============================================================================
imp.configure("Hackathon social counter v" + VERSION, [], []);
log("At start I have available memory: " + imp.getmemoryfree());
log("I'm alive and at version " + VERSION + ", my id is " + hardware.getimpeeid() + " and my mac is " + imp.getmacaddress());
if ("getsoftwareversion" in imp) log("Imp firmware version: " + imp.getsoftwareversion())

score_board <- the_hardware();
text <- "";

// If the text has changed, then update the screen
agent.on("set_text", function(_text) {
  text = _text;
})

// Welcome message to start the loops
function finished_loop() {
  score_board.set_text(text, true, finished_loop);
}
score_board.set_text("Loading...", true, finished_loop);

// We are loaded, now tell the agent to send some data, regularly.
function ready() {
  imp.wakeup(60, ready);
  agent.send("ready", true);
}
ready();


