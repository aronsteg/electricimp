/*
Copyright (C) 2013 Electric Imp, Inc
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files 
(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/


// -----------------------------------------------------------------------------
// Timer class: Implements a simple timer class with one-off and interval timers
//              all of which can be cancelled.
//
// Author: Aron
// Created: October, 2013
//
// =============================================================================
class timer {

    cancelled = false;
    paused = false;
    running = false;
    callback = null;
    interval = 0;
    params = null;
    send_self = false;
    static timers = [];

    // -------------------------------------------------------------------------
    constructor(_params = null, _send_self = false) {
        params = _params;
        send_self = _send_self;
        timers.push(this); // Prevents scoping death
    }

    // -------------------------------------------------------------------------
    function _cleanup() {
        foreach (k,v in timers) {
            if (timers == this) return timers.remove(k);
        }
    }
    
    // -------------------------------------------------------------------------
    function update(_params) {
        params = _params;
        return this;
    }

    // -------------------------------------------------------------------------
    function set(_duration, _callback) {
        assert(running == false);
        callback = _callback;
        running = true;
        imp.wakeup(_duration, alarm.bindenv(this))
        return this;
    }

    // -------------------------------------------------------------------------
    function repeat(_interval, _callback) {
        assert(running == false);
        interval = _interval;
        return set(_interval, _callback);
    }

    // -------------------------------------------------------------------------
    function cancel() {
        cancelled = true;
        return this;
    }

    // -------------------------------------------------------------------------
    function pause() {
        paused = true;
        return this;
    }

    // -------------------------------------------------------------------------
    function unpause() {
        paused = false;
        return this;
    }

    // -------------------------------------------------------------------------
    function alarm() {
        if (interval > 0 && !cancelled) {
            imp.wakeup(interval, alarm.bindenv(this))
        } else {
            running = false;
            _cleanup();
        }

        if (callback && !cancelled && !paused) {
            if (!send_self && params == null) {
                callback();
            } else if (send_self && params == null) {
                callback(this);
            } else if (!send_self && params != null) {
                callback(params);
            } else  if (send_self && params != null) {
                callback(this, params);
            }
        }
    }
}


// -----------------------------------------------------------------------------
// Firebase class: Implements the Firebase REST API.
// https://www.firebase.com/docs/rest-api.html
//
// Author: Aron
// Created: September, 2013
//
class Firebase {

    database = null;
    authkey = null;
    agentid = null;
    url = null;
    headers = null;

    // ........................................................................
    constructor(_database, _authkey, _path = null) {
        database = _database;
        authkey = _authkey;
        agentid = http.agenturl().slice(-12);
        headers = {"Content-Type": "application/json"};
    	set_path(_path);
    }


    // ........................................................................
	function set_path(_path) {
		if (!_path) {
			_path = "agents/" + agentid;
		}
        url = "https://" + database + ".firebaseIO.com/" + _path + ".json?auth=" + authkey;
	}


    // ........................................................................
    function write(data = {}, callback = null) {

        if (typeof data == "table") data.heartbeat <- time();
        http.request("PUT", url, headers, http.jsonencode(data)).sendasync(function(res) {
            if (res.statuscode != 200) {
                if (callback) callback(res);
                else server.log("Write: Firebase response: " + res.statuscode + " => " + res.body)
            } else {
                if (callback) callback(null);
            }
        }.bindenv(this));

    }

    // ........................................................................
    function update(data = {}, callback = null) {

        if (typeof data == "table") data.heartbeat <- time();
        http.request("PATCH", url, headers, http.jsonencode(data)).sendasync(function(res) {
            if (res.statuscode != 200) {
                if (callback) callback(res);
                else server.log("Update: Firebase response: " + res.statuscode + " => " + res.body)
            } else {
                if (callback) callback(null);
            }
        }.bindenv(this));

    }

    // ........................................................................
    function push(data, callback = null) {

        if (typeof data == "table") data.heartbeat <- time();
        http.post(url, headers, http.jsonencode(data)).sendasync(function(res) {
            if (res.statuscode != 200) {
                if (callback) callback(res, null);
                else server.log("Push: Firebase response: " + res.statuscode + " => " + res.body)
            } else {
                local body = null;
                try {
                    body = http.jsondecode(res.body);
                } catch (err) {
                    if (callback) return callback(err, null);
                }
                if (callback) callback(null, body);
            }
        }.bindenv(this));

    }

    // ........................................................................
    function read(callback = null) {
        http.get(url, headers).sendasync(function(res) {
            if (res.statuscode != 200) {
                if (callback) callback(res, null);
                else server.log("Read: Firebase response: " + res.statuscode + " => " + res.body)
            } else {
                local body = null;
                try {
                    body = http.jsondecode(res.body);
                } catch (err) {
                    if (callback) return callback(err, null);
                }
                if (callback) callback(null, body);
            }
        }.bindenv(this));
    }

    // ........................................................................
    function remove(callback = null) {
        http.httpdelete(url, headers).sendasync(function(res) {
            if (res.statuscode != 200) {
                if (callback) callback(res);
                else server.log("Delete: Firebase response: " + res.statuscode + " => " + res.body)
            } else {
                if (callback) callback(null, res.body);
            }
        }.bindenv(this));
    }

}


// -----------------------------------------------------------------------------
fb <- Firebase("devices", "LLfj2iAIHqwwy4mrsci8JprdU6U31HgXDbVEiz7A");




//----------------------------------------------------------------------
function remote(dev, method, params=[], callback=null, clear=true) {
  
    // Convert strings to tables
    if (typeof params == "string") {
        params = [params];
    }
    
    // Set a temporary event handler
    local event = dev + "." + method;
    device.on(event, function(res) {
        // Clear the old event handler and call the callback
        if (clear) device.on(event, function(d){});
        if (callback) callback(res);
    });
    
    // Send the request to the device
    device.send(dev, {method=method, params=params});
}

//----------------------------------------------------------------------
position <- {updated = false};
function read_remote_data(dummy = null) {
 
    // Send continuous stream of changes to the position of the Nora. Won't wake up Nora.
    local threshold = 0.3;
    local thresholds = { low = -threshold, high = threshold, axes = "XYZ"};
    remote("accelerometer", "read", [], function (res) {
        if (res) {
            position <- res;
            position.updated <- true;

            // server.log(format("Acceleration: [X: %0.02f, Y: %0.02f, Z: %0.02f]", res.x, res.y, res.z));
        }
        imp.wakeup(0.3, read_remote_data);
    })
}


//----------------------------------------------------------------------
// Check for an updated position and send it
function update_firebase() {
    if (position.updated) {
        fb.update(position);
        position.updated = false
    } 
}
timer().repeat(0.33, update_firebase);

//----------------------------------------------------------------------
device.on("ready", read_remote_data);
server.log("Agent started")

