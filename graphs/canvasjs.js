
// Setup the global variables
var params = $.url().param();
var agentid = params['agent'].slice(-12);
var fburl = "https://devices.firebaseIO.com/agents/" + agentid + "/history";
var fb = new Firebase(fburl);
const MAXIMUM_DISPLAY_POINTS = 720;

window.onload = function() {

	// Initialise the chart
	var dataTemperature = [], dataLight = [], dataBattery = [], dataHumidity = [], dataPressure = [];

	var chart1 = new CanvasJS.Chart("chartContainer1",{
		zoomEnabled: true,
		animationEnabled: true,
		title: {
			text: "Data from Nora The Explorer"		
		},
		toolTip: {
			shared: true
		},
		legend: {
			verticalAlign: "top",
			horizontalAlign: "center",
			fontSize: 14,
			fontWeight: "bold",
			fontFamily: "calibri",
			fontColor: "dimGrey"
		},
		axisX: {
			title: "Time"
		},
		axisY:{
			prefix: '',
			includeZero: false
		}, 
		data: [
			{ type: "stepLine", markerSize: 0, xValueType: "dateTime", showInLegend: true, name: "Temperature", dataPoints: dataTemperature },
			{ type: "spline", xValueType: "dateTime", showInLegend: true, name: "Light", axisYType: "secondary", dataPoints: dataLight },
		]
	});



	// pushing the new values
	function appendData(data) {
		// console.log(data);
		var time = new Date(data.heartbeat * 1000).getTime();
		dataTemperature.push({ x: time, y: data.temp }); 	if (dataTemperature.length > MAXIMUM_DISPLAY_POINTS) dataTemperature.shift();
		dataPressure.push({ x: time, y: data.pressure });	if (dataPressure.length > MAXIMUM_DISPLAY_POINTS) dataPressure.shift();
		dataLight.push({ x: time, y: data.ambient });		if (dataLight.length > MAXIMUM_DISPLAY_POINTS) dataLight.shift();
		dataHumidity.push({ x: time, y: data.humidity });	if (dataHumidity.length > MAXIMUM_DISPLAY_POINTS) dataHumidity.shift();
		dataBattery.push({ x: time, y: data.battery });		if (dataBattery.length > MAXIMUM_DISPLAY_POINTS) dataBattery.shift();
	}


	// Setup a function that disappears for a while
	function updateDisplay() {
		// updating legend text
		chart1.render();
	}

	// Read the first 12 hours of data and then all new data after that.
	var data_counter = 0;
	var last_name = null;
	fb.limit(MAXIMUM_DISPLAY_POINTS).once('value', function(snapshot) {

		// We have the historical data, process it
		snapshot.forEach(function(childSnapshot) {
			var data = childSnapshot.val();
			appendData(data);
			last_name = childSnapshot.name();
			++data_counter;
		});
		console.log("Finished loading " + data_counter + " historical items at: " + last_name);

		// Only update the display at the end of loading the data
		updateDisplay();

		// Now read all new data as it comes in and process it
		fb.startAt(null, last_name).limit(MAXIMUM_DISPLAY_POINTS).on('child_added', function(snapshot) {

			if (snapshot.name() == last_name) return;

			var data = snapshot.val();
			appendData(data);
			updateDisplay();
			console.log("Update: " + (++data_counter) + " => " + snapshot.name());

		});

	});
}

